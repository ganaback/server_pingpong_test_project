FROM node:8.10.0

MAINTAINER jeongrae Kim <kimjr@gananetworks.com>

RUN mkdir -p /app
WORKDIR /app/src
ADD . /app
RUN npm install
ENV NODE_ENV development

EXPOSE 3000
CMD ["npm","start"]

