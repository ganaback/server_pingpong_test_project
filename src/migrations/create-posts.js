'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('posts', {
          id:{
            allowNull: false,
            autoIncrement: true,
            primaryKey: true,
            type: Sequelize.INTEGER
          },
          postUuid: {
            allowNull :false,
            // unique : true,
            type: Sequelize.STRING,
            validate :{
              notEmpty: true
            }
          },
          acceptedAnswerId: {
            type: Sequelize.STRING,
          },
          parentID: {
            type: Sequelize.STRING,
          },
          voteCount: {
            type: Sequelize.INTEGER
          },
          viewCount: {
            type: Sequelize.INTEGER
          },
          body: {
            type: Sequelize.STRING(1024)
          },
          title: {
            type: Sequelize.STRING
          },
          ownerUserId: {
            type: Sequelize.STRING
          },
          ownerUserName: {
            type: Sequelize.STRING
          },
          categoryId :{
            type : Sequelize.STRING
          },
          answerCount:{
            type : Sequelize.INTEGER
          },
          closedDate:{
              type:Sequelize.DATE
          }
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('posts');
  }
};