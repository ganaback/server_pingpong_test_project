'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('files', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      memUuid: {
        type: Sequelize.STRING
      },
      postUuid: {
        type: Sequelize.STRING
      },
      fiUuid: {
        type: Sequelize.STRING
      },
      fiOriginname: {
        type: Sequelize.STRING
      },
      fiSize: {
        type: Sequelize.INTEGER
      },
      fiType: {
        type: Sequelize.STRING
      },
      fiUsedType: {
        type: Sequelize.INTEGER
      },      
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      supportId:Sequelize.STRING(36)
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('files');
  }
};