'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('budtenders', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      mem_uuid: {
        // unique : true,
         type: Sequelize.STRING,
         // ,
         // allowNull: false,
         references : {model : 'members', key: 'mem_uuid'},
             },
       postUuid: Sequelize.STRING,
       budMessage: Sequelize.STRING,
       budCode: Sequelize.STRING,
       budState: Sequelize.INTEGER,
       fiUuid: Sequelize.STRINR,
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('budtenders');
  }
};