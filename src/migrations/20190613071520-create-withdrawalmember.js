'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('withdrawalmembers', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      mem_email: {
        allowNull :false,
        unique : true,
        type: Sequelize.STRING,
        validate :{
          isEmail:true,
          notEmpty: true
        }
      },
      mem_pw: {
        allowNull :false,
        type: Sequelize.STRING,
        validate :{        
          notEmpty: true
        }
      },
      salt: {
        type: Sequelize.STRING,
        comment : "pw"
      },
      mem_nickname: {
        unique : true,
        type: Sequelize.STRING
      },
      mem_country: {
        type: Sequelize.STRING
      },
      mem_state: {
        type: Sequelize.STRING
      },
      mem_birth: {
        type: Sequelize.DATE
      },
      mem_type: {
        type: Sequelize.STRING
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      mem_uuid :{
        type : Sequelize.STRING
      },
      sns_type:{
        type : Sequelize.INTEGER
      },
      answer:{
        type : Sequelize.INTEGER
      },
      mem_droptime: {
        type: Sequelize.DATE
      },
      mem_jointime: {
        type: Sequelize.DATE
      },
      mem_profile :{
        type : Sequelize.STRING
      },
      mem_profileBg :{
        type : Sequelize.STRING
      },
      mem_info :{
        type : Sequelize.STRING
      },
      mem_interested :{
        type : Sequelize.STRING
      },
      mem_links :{
        type : Sequelize.STRING
      },
      mem_sub_email :{
        type : Sequelize.STRING
      },
      mem_last_updatedAt :{
        type : Sequelize.DATE
      },
      mem_notifications :{
        type : Sequelize.STRING
      },
      mem_sns_open:{
        type : Sequelize.INTEGER
      },
      mem_search_agree:{
        type : Sequelize.INTEGER
      },
      mem_use_data_agree:{
        type : Sequelize.INTEGER
      }
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('withdrawalmember');
  }
};