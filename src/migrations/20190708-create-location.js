'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('locations', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      mem_uuid: {
        type: Sequelize.STRING
      },
      locaUuid: {
        allowNull :false,
        // unique : true,
        type: Sequelize.STRING,
        validate :{
          notEmpty: true
        }
      },   
      startYear: {
        type: Sequelize.DATE
      },
      endYear: {
        type: Sequelize.DATE
      },
      isLive: {
        type: Sequelize.BOOLEAN
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('locations');
  }
};