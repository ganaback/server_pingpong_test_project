'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('employments', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
       mem_uuid: {
        type: Sequelize.STRING
      },
      empUuid: {
        allowNull :false,
        // unique : true,
        type: Sequelize.STRING,
        validate :{
          notEmpty: true
        }
      },
     
      position: {
        type: Sequelize.STRING
      },
      company: {
        type: Sequelize.STRING
      },
      startYear: {
        type: Sequelize.DATE
      },
      endYear: {
        type: Sequelize.DATE
      },
      isWork: {
        type: Sequelize.BOOLEAN
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('employments');
  }
};