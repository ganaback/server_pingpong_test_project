const express = require("express");
const router = express.Router();

import filesql from "../sql/file_sql";
import msql from "../sql/member_sql";
import budsql from "../sql/budtender_sql";
import fileutil from "../../../utils/fileupload";
import util from "../util.js";
import { promises } from "fs";

//버드텐더 취소
router.get("/cancel", function(req, res, next) {
  const id = util.getId(req.headers["x-access-token"]);
  budsql
    .getbudstatus(id)
    .then(result => {
      if (result.info == 2||result.info==1) {
        return true;
      } else {
        res.json(Object.assign({result:false}, result));
        return false;
      }
    })
    .then(result => {
      if (result) {
        budsql.setbudstatus(id, 4)
        .then(result=>{
          util.createLog(82,id,id,req);
          res.json({ result: true, message: "cancel success" });
        })
        .catch(err=>{
          res.json({ result: false, message: err });
        })
      }
    });
});


//버드텐더 재신청
router.get("/reapply", function(req, res, next) {
  const id = util.getId(req.headers["x-access-token"]);

  budsql
    .getbudstatus(id)
    .then(result => {
      if (result.info == 2||result.info==4) {
        return true;
      } else {
        res.json(Object.assign({result:false}, result));
        return false;
      }
    })
    .then(result => {
      if (result) {
        const bud = budsql.findOneId(id);
        const files = filesql.findOneBudtender(id);

        Promise.all([bud, files])
          .then(result => {
            let outfiles = new Array();
            for (let i = 0; i < result[1].length; i++) {
              let fileJson = new Object();
              fileJson.fileUuid = result[1][i].fiUuid;
              fileJson.fileOutput =
                result[1][i].fiDestination + result[1][i].fiFilename;
              outfiles.push(fileJson);
            }
            
            res.json({ result: true, budtender: result[0], files: outfiles });
          })
          .catch(err => {
            res.json({ result: false, message: err + "" });
          });
      }
    });
});

const upload = fileutil.certification();

router.post("/reapply", function(req, res, next) {
  const id = util.getId(req.headers["x-access-token"]);

  budsql
    .getbudstatus(id)
    .then(result => {
      if (result.info == 2||result.info ==4) {
        return true;
      } else {
        res.json(Object.assign({result:false}, result));
        return false;
      }
    })
    .then(result => {
      if (!result) {
      } else {
        upload.array("certification", 3)(req, res, function(err) {
          let fiUuid = req.body.fiuuid;
          let fiarr = fiUuid.split(",");
          let delfiuuid = req.body.delfiuuid;
          let delarr = delfiuuid.split(",");
          const files = req.files;
          const ficount = fiarr.length + files.length;
          if (req.fileValidationError) {
            res.json({
              result: false,
              message: req.fileValidationError,
              info:5
            });
          } else if (err) {
            //3개 초과시 에러
            res.json({ result: false, message: err + "", info:6});
          } else if (
            (ficount > 4 && fiUuid == "") ||
            (ficount > 3 && fiUuid != "")
          ) {
            //console.log(files.length);
            for (let i = 0; i < files.length; i++) {
              //console.log(files[i].path);
              fileutil.deletefileOnfile(files[i].path);
            }
            res.json({
              result: false,
              message: "기존등록+추가파일이 3개가 넘음",
              info:7
            });
          } else {
            //파일 삭제
            let delfile = new Array();
            for (let i = 0; i < delarr.length; i++) {
              //console.log(delarr[i])
              delfile[i] = filesql.findOneFiUuid(delarr[i]);
              //delresult[i] =filesql.deletefile(delarr[i]);
            }
            //파일 삭제
            Promise.all([...delfile]).then(result => {
              if (result[0] != null) {
                for (let i = 0; i < result.length; i++) {
                  fileutil.deletefile(result[i]);
                  filesql.deletefile(result[i].fiUuid).then(result => {});
                }
                console.log(result.length + "개 db 파일 삭제성공");
              }
            });

            //추가파일 업로드
            let result = new Array();
            for (let i = 0; i < files.length; i++) {
              result[i] = filesql.upload(id, null, files[i], 3);
            }

            Promise.all([...result]).then(file => {
              const body = req.body;
              const bud = budsql
                .reapply(id, body)
                .then(result => {
                  //res.send(result);
                  util.createLog(81,id,id,req);
                  res.json({ result: true, message: result });
                })
                .catch(err => {
                  res.json({ result: false, message: err + "" });
                });
            });
          }
        });
      }
    });
});

module.exports = router;
