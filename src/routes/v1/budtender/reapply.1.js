const express = require("express");
const router = express.Router();

import filesql from "../sql/file_sql";
import msql from "../sql/member_sql";
import budsql from "../sql/budtender_sql";
import fileutil from "../../../utils/fileupload";
import util from "../util.js";
import { promises } from "fs";

//버드텐더 재신청
router.get("/reapply", function(req, res, next) {
  const id = util.getId(req.headers["x-access-token"]);
  budsql.getbudstatus(id)
  .then(budstatus => {
    if (!budstatus.result) {
      res.json(budstatus);
    } else {
      budsql
        .findOneId(id)
        .then(result => {
          if (result) {
            if (result.fiUuid) {
              const fiuu = result.fiUuid;
              let arr = fiuu.split(",");
              let filearr = new Array();
              arr.forEach(function(item, index) {
                //filearr.put(item);
                filearr[index] = filesql.findOneFiUuid(item);
              });

              Promise.all([...filearr])
                .then(resultfile => {
                  let outfiles = new Array();

                  for (let i = 0; i < resultfile.length; i++) {
                    let fileJson = new Object();
                    fileJson.fileUuid = resultfile[i].fiUuid;
                    fileJson.fileOutput =
                      resultfile[i].fiDestination + resultfile[i].fiFilename;
                    outfiles.push(fileJson);
                  }
                  res.json({
                    result: true,
                    message: result,
                    fileinfo: outfiles
                  });
                })
                .catch(err => {
                  res.send(err + "");
                });
            } else {
              res.json({ result: true, message: result });
            }
          } else {
            res.json({ result: false, message: "미신청" });
          }
        })
        .catch(err => {
          res.json({ result: false, message: err + "" });
        });
    }
  });
});

const upload = fileutil.certification();

router.post("/reapply", function(req, res, next) {
  const id = util.getId(req.headers["x-access-token"]);
  budsql
    .getbudstatus(id)
    //파일 업로드
    .then(budstatus => {
      if (!budstatus.result) {
        res.json(budstatus);
      } else {
        upload.array("certification", 3)(req, res, function(err) {
          let fiUuid = req.body.fiuuid;
          let fiarr = fiUuid.split(",");
          let delfiuuid = req.body.delfiuuid;
          let delarr = delfiuuid.split(",");
          const files = req.files;
          const ficount = fiarr.length + files.length;
          if (req.fileValidationError) {
            res.json({
              result: false,
              message: req.fileValidationError,
              info: 4
            });
          } else if (err) {
            //3개 초과시 에러
            res.json({ result: false, message: err + "", info: 5 });
          } else if (
            (ficount > 4 && fiUuid == "") ||
            (ficount > 3 && fiUuid != "")
          ) {
            //console.log(files.length);
            for (let i = 0; i < files.length; i++) {
              //console.log(files[i].path);
              fileutil.deletefileOnfile(files[i].path);
            }
            res.json({
              result: false,
              message: "기존등록+추가파일이 3개가 넘음",
              info: 6
            });
          } else {
            //파일 삭제
            let delfile = new Array();
            for (let i = 0; i < delarr.length; i++) {
              //console.log(delarr[i])
              delfile[i] = filesql.findOneFiUuid(delarr[i]);
              //delresult[i] =filesql.deletefile(delarr[i]);
            }
            //파일 삭제
            Promise.all([...delfile]).then(result => {
              if (result[0] != null) {
                for (let i = 0; i < result.length; i++) {
                  fileutil.deletefile(result[i]);
                  filesql.deletefile(result[i].fiUuid).then(result => {});
                }
                console.log(result.length + "개 db 파일 삭제성공");
              }
            });

            //추가파일 업로드
            let result = new Array();
            for (let i = 0; i < files.length; i++) {
              result[i] = filesql.upload(id, null, files[i], 3);
            }
            //fiuuid 합치기
            if (files.length > 0 && fiUuid != "") {
              fiUuid += ",";
            }
            Promise.all([...result]).then(file => {
              file.forEach(function(item, index) {
                fiUuid += item.fiUuid + ",";
              });
              if (files.length > 0) {
                fiUuid = fiUuid.substr(0, fiUuid.length - 1);
              }

              const body = req.body;
              const bud = budsql.reapply(id, body, fiUuid);
              const m = msql.budtender(id, 1);

              //member,budtender update
              Promise.all([bud, m])
                .then(result => {
                  //res.send(result);
                  res.json({ result: true, message: result });
                })
                .catch(err => {
                  res.json({ result: false, message: err + "" });
                });
            });
          }
        });
      }
    });
});

module.exports = router;
