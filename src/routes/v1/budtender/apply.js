const express = require("express");
const router = express.Router();
const fs = require("fs");
const multer = require("multer");
const path = require("path");

import filesql from "../sql/file_sql";
import msql from "../sql/member_sql";
import budsql from "../sql/budtender_sql";
import artsql from "../sql/article_sql";
import fileutil from "../../../utils/fileupload";
import util from "../util.js";
import { promises } from "fs";

// upload 페이지 경로
router.get("/apply", function(req, res, next) {
  res.render("board1");
});

const upload = fileutil.certification();
// router.post("/123", function(req, res, next){

// })

//const upload = upload.array("certification", 3);

//버드텐더 신청
router.post("/apply", function(req, res, next) {
  const id = util.getId(req.headers["x-access-token"]);

  budsql.getbudstatus(id)
  .then(result =>{
    if(result.info==0){
      return true;
    }else{
      res.json(Object.assign({result:false}, result));
      return false;      
    }
  })
  .then(result=>{
    if(result){
      artsql.findOneId(id)
      .then(article=>{
        if(article){
          upload.array("certification", 3)(req, res, function(err) {
   
            if (req.fileValidationError) {
              res.json({ result: false, message: req.fileValidationError, info:5 });
            }else  if (err) {
              //3개 초과시 에러
              res.json({ result: false, message: err + "" , info:6});
            }else{
        
            const files = req.files;
        
            let input = new Array();
            for (let i = 0; i < files.length; i++) {
              input[i] = filesql.upload(id, null, files[i], 3);
            }
        
            const body = req.body;
            body.article =  article.articleUuid;
            const bud = budsql.apply(id, body);
    
            input.push(bud);
        
            Promise.all(input)
            .then(result => {
              util.createLog(80,id,id,req);
              res.json({result:true, message:result});
            })
            .catch(err=>{
              res.json({result:false, message:err});
            })
          }
          });

          //끝
        }else{
          res.json({result:false, info:7});
        }
      })
      .catch(err=>{
        res.json({result:false, message:err});
      })
    }
  })
});

//임시로 셋팅 바꾸기
router.post("/set", function(req, res, next) {
  const id = util.getId(req.headers["x-access-token"]);
  const type = req.body.type;
  msql.budtender(id, type)
  .then(result=>{
    res.send(result)
  })
  .catch(err=>{
    res.send(err)
  })
})

//임시로 셋팅 바꾸기
router.get("/get", function(req, res, next) {
  const id = util.getId(req.headers["x-access-token"]);
  artsql.findOneId(id)
  .then(result=>{
    res.send(result)
  })
  .catch(err=>{
    res.send(err)
  })
})

//버드텐더 신청
router.post("/apply2", function(req, res, next) {
  const body = req.body;
  console.log(body);
  const bud = budsql.apply(body);

  const m = msql.budtender(body.id, 1);

  Promise.all([bud, m])
    .then(result => {
      res.send(result);
    })
    .catch(err => {
      res.send(err);
    });
});

module.exports = router;
