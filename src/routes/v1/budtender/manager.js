const express = require("express");
const router = express.Router();

import filesql from "../sql/file_sql";
import msql from "../sql/member_sql";
import budsql from "../sql/budtender_sql";
import fileutil from "../../../utils/fileupload";
import util from "../util.js";
import { promises } from "fs";

//버드텐더 승인
router.get("/approval", function(req, res, next) {
  const id = req.body.id;

  const bud = budsql.setbudstatus(id, 3);
  const m = msql.budtender(id, 1);

    Promise.all([bud, m])
        .then(result=>{
          util.createLog(84,id,id,req);
          res.json({ result: true, message: "approval success" });
        })
        .catch(err=>{
          res.json({ result: false, message: err });
        })
});

//버드텐더 반려
router.get("/return", function(req, res, next) {
    const id = req.body.id;
  
    budsql.setbudstatus(id, 2)
          .then(result=>{
            util.createLog(83,id,id,req);
            res.json({ result: true, message: "return success" });
          })
          .catch(err=>{
            res.json({ result: false, message: err });
          })
  });



module.exports = router;
