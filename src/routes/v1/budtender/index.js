module.exports = function(app){//함수로 만들어 객체 app을 전달받음
	var express = require('express');
	var router = express.Router();

	var apply = require('./apply');
	app.use("/budtender" , apply);

	app.use("/budtender", require('./confirm'));
	app.use("/budtender", require('./reapply'));
	app.use("/budtender", require('./manager'));
	app.use("/budtender", require('./searchCategory'));

	app.use("/budtender", require('./jointest'));
	

	
	return router;	//라우터를 리턴
};