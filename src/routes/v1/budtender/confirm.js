const express = require("express");
const router = express.Router();

const util = require("../util");
import msql from "../sql/member_sql";
import filesql from "../sql/file_sql";
import budsql from "../sql/budtender_sql";
import { promises } from "fs";
import models, { Sequelize } from "../../../models";

router.get("/confirm", (req, res, next) => {
  const id = util.getId(req.headers["x-access-token"]);
  const bud = budsql.findOneId(id);
  const files = filesql.findOneBudtender(id);

  budsql
    .getbudstatus(id)
    .then(result => {
      if (result.info == 0) {
        res.json(Object.assign({ result: false }, result));
        return false;
      } else {
        return true;
      }
    })
    .then(result => {
      if (result) {
        Promise.all([bud, files])
          .then(result => {
            let outfiles = new Array();
            for (let i = 0; i < result[1].length; i++) {
              let fileJson = new Object();
              fileJson.fileUuid = result[1][i].fiUuid;
              fileJson.fileOutput =
                result[1][i].fiDestination + result[1][i].fiFilename;
              fileJson.originFileName = result[1][i].fiOriginname;
              outfiles.push(fileJson);
            }
            res.json({ result: true, budtender: result[0], files: outfiles });
          })
          .catch(err => {
            res.json({ result: false, message: err + "" });
          });
      }
    });
});

router.get("/confirm2", (req, res, next) => {
  const id = util.getId(req.headers["x-access-token"]);

  budsql.findOneId(id).then(result => {
    if (result) {
      res.json({ result: true, info: result });
    } else {
      res.json({ result: fale, info: "미신청" });
    }
  });
});

module.exports = router;
