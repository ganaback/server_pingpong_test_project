const express = require("express");
const router = express.Router();
const fs = require("fs");
const multer = require("multer");
const path = require("path");

import filesql from "../sql/file_sql";
import msql from "../sql/member_sql";
import budsql from "../sql/budtender_sql";

import util from "../util.js";
import { promises } from "fs";

let storage = multer.diskStorage({
  destination: function(req, file, callback) {
    callback(null, "certification/");
  },
  filename: function(req, file, callback) {
    let extension = path.extname(file.originalname);
    let basename = path.basename(file.originalname, extension);
    callback(null, basename + "-" + Date.now() + extension);
  }
});

let upload = multer({
  storage: storage
});

// upload 페이지 경로
router.get("/apply", function(req, res, next) {
  res.render("board1");
});

//버드텐더 신청
router.post("/apply", upload.array("certification", 3), function(
  req,
  res,
  next
) {
  const files = req.files;
  const id = req.body.id;

  let result = new Array();
  for (let i = 0; i < files.length; i++) {
    result[i] = filesql.upload(id, null, files[i], 3);
    console.log(result[i]);
  }

  let fiUuid = "";
  Promise.all([result[0], result[1], result[2]]).then(file => {
    file.forEach(function(item, index) {
      if (item) {
        console.log(fiUuid);
        fiUuid += item.fiUuid + ",";
      }
    });
    fiUuid = fiUuid.substr(0, fiUuid.length - 1);

    const body = req.body;
    const bud = budsql.apply(body, fiUuid);
    const m = msql.budtender(body.id, 1);

    Promise.all([bud, m])
      .then(result => {
        res.send(result);
      })
      .catch(err => {
        res.send(err);
      });
  });
});

//버드텐더 신청
router.post("/apply2", function(req, res, next) {
  const body = req.body;
  console.log(body);
  const bud = budsql.apply(body);

  const m = msql.budtender(body.id, 1);

  Promise.all([bud, m])
    .then(result => {
      res.send(result);
    })
    .catch(err => {
      res.send(err);
    });
});

module.exports = router;
