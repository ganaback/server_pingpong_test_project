const express = require("express");
const router = express.Router();
const fs = require("fs");
const multer = require("multer");
const path = require("path");
const Sequelize = require("sequelize");
const Op = Sequelize.Op;
import filesql from "../sql/file_sql";
import msql from "../sql/member_sql";
import budsql from "../sql/budtender_sql";
import artsql from "../sql/article_sql";
import fileutil from "../../../utils/fileupload";
import util from "../util.js";
import { promises } from "fs";
const models = require("../../../models");
import sequelize from "sequelize";
const con = models.sequelize;

router.get("/category/:category", (req, res, next) => {
  const category = req.params.category;
  const sel = `select * from members  where FIND_IN_SET (${category}, mem_interested)>0 and budtender =1 LIMIT 10`;
  let fileArr = new Array();
  let locationArr = new Array();
  let eduArr = new Array();
  let empArr = new Array();
  con
    .query(sel)
    .then(([result, metadata]) => {
      for (let i = 0; i < result.length; i++) {
        locationArr[i] = models.location.findAll({
          where: { mem_uuid: result[i].mem_uuid }
        });
        eduArr[i] = models.education.findAll({
          where: { mem_uuid: result[i].mem_uuid }
        });
        empArr[i] = models.employment.findAll({
          where: { mem_uuid: result[i].mem_uuid }
        });
        if (result[i].mem_profile) {
          fileArr[i] = filesql.findOneFiUuid(result[i].mem_profile);
        }
      }
      return result;
    })
    .then(result => {
      const arr = fileArr.concat(locationArr, eduArr, empArr);

      Promise.all(arr).then(file => {
        for (let i = 0; i < result.length; i++) {
          delete result[i].mem_pw;
          delete result[i].salt;
          result[i].profile = file[i];
          result[i].location = file[i + result.length];
          result[i].education = file[i + result.length * 2];
          result[i].employment = file[i + result.length * 3];
        }
        //res.send(result);
        res.json({ result: true, message: result });
      });
    })
    .catch(err => {
      res.json({ result: false, message: "Server error" + err });
    });
});

module.exports = router;
