/*********************
 * @author 김정래
 * 작성일 : 2019-06-27
 * POST 질문/답변 전용 controller
 *********************/
import models, { Sequelize } from '../../../models'
import sequelize from 'sequelize';
import jwtverify from '../../../utils/jwtverify'

const con = models.sequelize;
const Op = Sequelize.Op;


//내 Inbox 보기
exports.inbox = (req, res, next) => {
    const token = req.headers['x-access-token'];
    try{
        const payload = jwtverify.verify(token); 


        var qstr = `
        SELECT a.*,b.name,b.NotiMessage,p.body,p.title
        FROM 
        logs a INNER JOIN logtype b ON a.logType = b.Id 
        LEFT JOIN posts p ON a.tableKey = p.postUuid
        WHERE 
        b.NotiMessage IS NOT NULL AND  
        receiveId = "${payload.id}" AND read_at IS NULL ORDER BY a.createdAt DESC
        `;

        con.query(qstr)
        .then(([result, metadata]) => {
            res.json({result:true,inbox:result});
        })
        .catch(err=>{
            res.json({result:false,message:"Server error"+err});
        }) 

  }catch(err){
      res.json({result:false,message:err.message});
  }
}

/** 버드텐더에게 알림 보내기**/
/* 
exports.toBudtender = (req,res,next) => {
    const token = req.headers['x-access-token'];
    try{
        const payload = jwtverify.verify(token); 
        var ip = req.headers['x-forwarded-for'] || req.connection.remoteAddress;
        const body = req.body;

        models.logs.create({
            logType:20,
            tableKey:body.postUuid,
            ip:ip,
            receiveId:body.receiveId
        })
        .then(([result, metadata]) => {
            res.json({result:true,inbox:result});
        })
        .catch(err=>{
            res.json({result:false,message:"Server error"+err});
        }) 

  }catch(err){
      res.json({result:false,message:err.message});
  }
}
 */