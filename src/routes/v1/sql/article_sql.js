const models = require("../../../models");

let article = {};


article.findOneId = function(id) {
  return new Promise((resolve, rejects) => {
    resolve(
      models.article.findOne({
        where: { mem_uuid: id }
      })
    );
  });
};
module.exports = article;
