const models = require("../../../models");

import util from "../util.js";
import { type } from "os";
import { rejects } from "assert";
const sequelize = require("sequelize");
const Op = sequelize.Op;
let moment = require("moment");
var msql = {};

//이메일로 찾기
msql.findOneEmail = function(email) {
  return new Promise((resolve, reject) => {
    resolve(models.member.findOne({ where: { mem_email: email } }));
  });
};

//nick으로 찾기
msql.findOneNickname = function(nickname) {
  return new Promise((resolve, reject) => {
    resolve(models.member.findOne({ where: { mem_nickname: nickname } }));
  });
};

//uuid로 찾기(pk)
msql.findOneUuid = function(uuid) {
  return new Promise((resolve, reject) => {
    resolve(models.member.findOne({ where: { mem_uuid: uuid } }));
  });
};

//uuid로 찾기(pk)
// msql.findOneUuidBudtender = function(uuid) {
  
//   return new Promise((resolve, reject) => {
//     resolve(models.member.findOne({ where: { mem_uuid: uuid } })
//       );
//   })  
// };

//회원정보 프론트로 보낼때 uuid로 찾기(pk) *비밀번호제외*
msql.findOneMyinfo = function(uuid) {
  return new Promise((resolve, reject) => {
    resolve(
      models.member.findOne({
        where: { mem_uuid: uuid },
        attributes: { exclude: ["mem_pw", "salt"] }
      })
    );
  });
};

//이메일 회원가입
msql.joinEmailMember = function(body) {
  let salt =
    Math.round(new Date().valueOf() * Math.random() * Math.random()) + "";

  let hashPassword = util.makeHashPw(body.pw, salt);

  let token = Math.round(new Date().valueOf() * Math.random() * Math.random());

  return new Promise((resolve, reject) => {
    resolve(
      models.member.create({
        mem_email: body.email,
        mem_pw: hashPassword,
        salt: salt,
        mem_token: token,
        mem_uuid: util.makeUuid4(),
        sns_type: 0,
        mem_type: 0,
        budtender: 0,
        mem_sub_email: body.email
      })
    );
  });
};

//미인증 회원 인증 메일 재발송
msql.updateTokenAndEmail = function(body, res) {
  let salt =
    Math.round(new Date().valueOf() * Math.random() * Math.random()) + "";
  let token = Math.round(new Date().valueOf() * Math.random() * Math.random());

  let hashPassword = util.makeHashPw(body.pw, salt);

  models.member
    .update(
      {
        mem_pw: hashPassword,
        salt: salt,
        mem_token: token
      },
      {
        where: { mem_email: body.email }
      }
    )
    .then(result => {
      util.joinEmailMember(body.email, token, res, "재");
    })
    .catch(err => {
     
      res.json({
        result: false,
        err: err + ""
      });
    });
};

//비밀번호 포함 정보 수정
msql.updateUserInfo = function(body) {
  let interestArr = body.interested;
      //console.log(interestArr);
      if(interestArr){
        interestArr = interestArr.map((val) =>{        
          return val.id;
        })
        interestArr = interestArr.join(',');  
      }
    
    let links = body.links;
    if(links){
      links = links.join(',');  
    }


  if (body.pw) {
    const salt = Math.round(new Date().valueOf() * Math.random()) + "";
    const pw = util.makeHashPw(body.pw, salt);
    return new Promise((resolve, reject) => {
      
      resolve(
        models.member.update(
          {
            mem_nickname: body.nickname,
            mem_country: body.country,
            mem_state: body.state,
            mem_birth: body.birth,
            mem_info: body.info,
            mem_interested: interestArr,
            mem_links: links,
            mem_sub_email: body.subemail,
            mem_notifications: body.noti,
            mem_sns_open: body.sns_open,
            mem_search_agree: body.search_agree,
            mem_use_data_agree: body.use_data_agree,
            mem_pw: pw,
            salt: salt
          },
          {
            where: { mem_uuid: body.id }
          }
        )
      );
    });
  } else {
    return new Promise((resolve, reject) => {
      resolve(
        models.member.update(
          {
            mem_nickname: body.nickname,
            mem_country: body.country,
            mem_state: body.state,
            mem_birth: body.birth,
            mem_info: body.info,
            mem_interested: interestArr,
            mem_links: 
            links,
            mem_sub_email: body.subemail,
            mem_notifications: body.noti,
            mem_sns_open: body.sns_open,
            mem_search_agree: body.search_agree,
            mem_use_data_agree: body.use_data_agree
          },
          {
            where: { mem_uuid: body.id }
          }
        )
      );
    });
  }
};

msql.updatePw = body => {
  if (!body.pw) {
    let salt = Math.round(new Date().valueOf() * Math.random()) + "";
    const pw = util.makeHashPw(body.pw, salt);
  }
  return new Promise((resolve, reject) => {
    resolve(
      models.member.update(
        {
          mem_pw: pw,
          salt: salt
        },
        {
          where: { mem_uuid: body.id }
        }
      )
    );
  });
};

//2차 회원가입
msql.updateDeail = function(body) {
  return new Promise((resolve, reject) => {
    resolve(
      models.member.update(
        {
          mem_nickname: body.nickname,
          mem_country: body.country,
          mem_state: body.state,
          mem_birth: body.birth
        },
        {
          where: { mem_uuid: body.id }
        }
      )
    );
  });
};

msql.updateToken = function(email, token) {
  return new Promise((resolve, reject) => {
    resolve(
      models.member.update(
        {
          mem_token: token
        },
        {
          where: {
            mem_email: email
          }
        }
      )
    );
  });
};

msql.findOneEmailAndToken = function(email, token) {
  return new Promise((resolve, reject) => {
    resolve(
      models.member.findOne({
        where: { mem_email: email, mem_token: token }
      })
    );
  });
};

//임시비번 발급
msql.updateTemporaryPw = function(email, token) {
  return new Promise((resolve, reject) => {
    let salt = Math.round(new Date().valueOf() * Math.random()) + "";
    const pw = util.makeHashPw(token, salt);
    resolve(
      models.member.update(
        {
          salt: salt,
          mem_pw: pw
        },
        {
          where: { mem_email: email, mem_token: token }
        }
      )
    );
  });
};

//인증회원
msql.updatType = function(uuid , type) {
  return new Promise((resolve, reject) => {
    resolve(
      models.member.update(
        {
          mem_type: type
        },
        {
          where: { mem_uuid: uuid }
        }
      )
    );
  });
};

//삭제할 휴먼계정찾기
msql.findDropMember = function() {
  return new Promise((resolve, reject) => {
    const date = moment().subtract(30, "days");
    //console.log(date);
    resolve(
      models.member.findAndCountAll({
        where: { mem_droptime: { [Op.lte]: date } }
      })
    );
  });
};

//회원탈퇴- 휴먼회원처리
msql.secession = function(uuid) {
  return new Promise((resolve, rejects) => {
    //console.log(new Date().getHours());
    resolve(
      models.member.update(
        {
          mem_droptime: new moment()
        },
        {
          where: { mem_uuid: uuid }
        }
      )
    );
  });
};

//탈퇴회원 멤버테이블에서 삭제
msql.dropMember = function(uuid) {
  return new Promise((resolve, rejects) => {
    resolve(
      models.member.destroy({
        where: { mem_uuid: uuid }
      })
    );
  });
};

//미인증 회원 멤버테이블에서 삭제할거 찾기
msql.dropType0Member = function(uuid) {
  return new Promise((resolve, rejects) => {
    const date = moment().subtract(1, "days");
    // console.log(date);
    resolve(
      models.member.findAndCountAll({
        where: { updatedAt: { [Op.lte]: date }, mem_type: 0 }
      })
    );
  });
};

//회원탈퇴- 휴먼회원 복귀처리
msql.comeback = function(uuid) {
  return new Promise((resolve, rejects) => {
    //console.log(new Date().getHours());
    resolve(
      models.member.update(
        {
          mem_droptime: null
        },
        {
          where: { mem_uuid: uuid }
        }
      )
    );
  });
};

//탈퇴회원 백업
//이메일 회원가입
msql.deleteMemberBackup = function(member) {
  return new Promise((resolve, reject) => {
    let dropmember = {
      mem_email: member.mem_email,
      mem_pw: member.mem_pw,
      salt: member.salt,
      mem_nickname: member.mem_nickname,
      mem_country: member.mem_country,
      mem_state: member.mem_state,
      mem_birth: member.mem_birth,
      mem_type: member.mem_type,
      mem_token: member.mem_token,
      mem_uuid: member.mem_uuid,
      sns_type: member.sns_type,
      budtender: member.budtender,
      mem_droptime: member.mem_droptime,
      mem_jointime: member.createdAt,
      mem_info: member.mem_info,
      mem_interested: member.mem_interested,
      mem_links: member.mem_links,
      mem_sub_email: member.mem_sub_email,
      mem_profile: member.mem_profile,
      mem_profileBg: member.mem_profileBg,
      mem_last_updatedAt: member.updatedAt,
      mem_notifications: member.mem_notifications,
      mem_sns_open: member.mem_sns_open,
      mem_search_agree: member.mem_search_agree,
      mem_use_data_agree: member.mem_use_data_agree
    };
    //console.log(dropmember);
    resolve(models.withdrawalmember.create(dropmember));
  });
};

//프로필 사진 등록
msql.profileUpload = function(uuid, fileUuid) {
  return new Promise((resolve, rejects) => {
    //console.log(new Date().getHours());
    resolve(
      models.member.update(
        {
          mem_profile: fileUuid
        },
        {
          where: { mem_uuid: uuid }
        }
      )
    );
  });
};

//프로필 배경 사진 등록
msql.profileBgUpload = function(uuid, fileUuid) {
  return new Promise((resolve, rejects) => {
    //console.log(new Date().getHours());
    resolve(
      models.member.update(
        {
          mem_profileBg: fileUuid
        },
        {
          where: { mem_uuid: uuid }
        }
      )
    );
  });
};

//프로필 사진 or 배경사진 등롱
msql.profileOrBgUpload = function(uuid, fileUuid, type) {
  return new Promise((resolve, rejects) => {
    if (type == 1) {
      resolve(
        models.member.update(
          {
            mem_profile: fileUuid
          },
          {
            where: { mem_uuid: uuid }
          }
        )
      );
    } else {
      resolve(
        models.member.update(
          {
            mem_profileBg: fileUuid
          },
          {
            where: { mem_uuid: uuid }
          }
        )
      );
    }
  });
};

msql.budtender = function(id, bud) {
  //console.log(id, bud);
  return new Promise((resolve, rejects) => {
    resolve(
      models.member.update(
        {
          budtender: bud
        },
        {
          where: { mem_uuid: id }
        }
      )
    );
  });
};


msql.getAllInfo = function(id){

  const m = models.member.findOne({
    where: { mem_uuid: id },
    attributes: { exclude: ["mem_pw", "salt"] }
  });

  const b = models.budtender.findOne({
    where: { mem_uuid: id }
  })

  const l =models.location.findAll({
    where: { mem_uuid: id }
  })

  const ed =models.education.findAll({
    where: { mem_uuid: id }
  })

  const em =models.employment.findAll({
    where: { mem_uuid: id }
  })


  return new Promise((resolve, rejects) => {
   
      Promise.all([m,b, l, ed, em])
      .then(result=>{
        resolve(result);
      })
      .catch(err=>{
        rejects(err)
      })
      
    
  });
 
  

}


module.exports = msql;
