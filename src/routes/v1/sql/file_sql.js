const models = require("../../../models");

import util from "../util.js";
import { rejects } from "assert";

let filesql = {};

filesql.upload = function(id, postuuid, file, fiUsedType) {
  return new Promise((resolve, rejects) => {
    resolve(
      models.file.create({
        mem_uuid: id,
        postUuid: postuuid,
        fiOriginname: file.originalname,
        fiFilename: file.filename,
        fiSize: file.size,
        fiType: file.mimetype,
        fiDestination: file.destination,
        fiUsedType: fiUsedType
      })
    );
  });
};


filesql.uploadAws = function(id, postuuid, file, fiUsedType, destination) {
  //console.log(file);
  console.log("filesql!!!!!!!!!!!!!")
  console.log(destination)
  return new Promise((resolve, rejects) => {
    resolve(
      models.file.create({
        mem_uuid: id,
        postUuid: postuuid,
        fiOriginname: file.name,
        fiFilename: file.fiFilename,
        fiSize: file.size,
        fiType: file.type,
        fiDestination: destination,
        fiUsedType: fiUsedType
      })
    );
  });
};

filesql.update = function(fiUuid, postuuid, file) {
    return new Promise((resolve, rejects) => {
      resolve(
        models.file.update({
          postUuid: postuuid,
          fiOriginname: file.originalname,
          fiFilename: file.filename,
          fiSize: file.size,
          fiType: file.mimetype,
          fiDestination: file.destination,
        },
        {
            where :
            {
                fiUuid: fiUuid
            }
        })
      );
    });
  };


filesql.findOneFiUuid = function(fiUuid) {
    return new Promise((resolve, rejects) => {
      resolve(
        models.file.findOne({
            where :
            {
                fiUuid : fiUuid
            }
        })
      );
    });
  };

  filesql.findOneMenUuid = function(id) {
    return new Promise((resolve, rejects) => {
      resolve(
        models.file.findOne({
            where :
            {
                mem_uuid : id
            }
        })
      );
    });
  };

  filesql.findOneBudtender = function(id) {
    return new Promise((resolve, rejects) => {
      console.log(id)
      resolve(
        models.file.findAll({
            where :
            {
                mem_uuid : id,
                fiUsedType:3
            }
        })
      );
    });
  };


  //파일 삭제
  filesql.deletefile = function(fiuuid) {
    return new Promise((resolve, rejects) => {
      resolve(
        models.file.destroy({
          where: { fiUuid: fiuuid }
        })
      );
    });
  };
module.exports = filesql;
