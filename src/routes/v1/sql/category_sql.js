const models = require("../../../models");

import util from "../util.js";


var categorySql = {};


categorySql.insertCategory = function(category){
    return new Promise((resolve, reject)=>{
        resolve(models.category.create({
            categoryName : category
        }))
    })
}

categorySql.selectId = function(id){
    //console.log(id)
    return new Promise((resolve, reject)=>{
        resolve(models.category.findOne({
            where :{ id : id}
        }))
    })
}

categorySql.selectCategory =function(){
    return new Promise((resolve, reject)=>{
        resolve(models.category.findAll())
    })
}

categorySql.selectCategoryWithCount =function(){
    return new Promise((resolve, reject)=>{
        resolve(models.category.findAndCountAll())
    })
}

module.exports = categorySql;