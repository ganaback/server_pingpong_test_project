const models = require("../../../models");

let budsql = {};

budsql.apply = function(id, body) {
  return new Promise((resolve, rejects) => {
    resolve(
      models.budtender.create({
        mem_uuid: id,
        articleUuid: body.article,
        budMessage: body.message,
        budCode: body.code,
        budState: 1
      })
    );
  });
};

budsql.findOneId = function(id) {
  return new Promise((resolve, rejects) => {
    resolve(
      models.budtender.findOne({
        where: { mem_uuid: id }
      })
    );
  });
};

//수정
budsql.reapply = function(id, body) {
  return new Promise((resolve, rejects) => {
    resolve(
      models.budtender.update(
        {
          articleUuid: body.postuuid,
          budMessage: body.message,
          budCode: body.code,
          budState: 1
        },
        {
          where: { mem_uuid: id }
        }
      )
    );
  });
};

budsql.setbudstatus = function(id, bud) {
  //console.log(id, bud);
  return new Promise((resolve, rejects) => {
    resolve(
      models.budtender.update(
        {
          budState: bud
        },
        {
          where: { mem_uuid: id }
        }
      )
    );
  });
};

budsql.getbudstatus = function(id) {
  return Promise.resolve(models.budtender.findOne({ where: { mem_uuid: id } }))
    .then(result => {
      if (!result) {
        return  {info:0, message:"미신청"};
      } else {
        if (result.budState == 1) {
          return {info:1, message:"승인대기"};
        } else if (result.budState == 2) {
          return  {info:2, message:"승인반려"};
        } else if (result.budState == 3) {
          return  {info:3, message:"승인완료"};
        } else if (result.budState == 4) {
          return {info:4, message:"신청취소"};
        }
      }
    })
    .catch(err => {
      return {err:err+""};
    });
};

module.exports = budsql;
