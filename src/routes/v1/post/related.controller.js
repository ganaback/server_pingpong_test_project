/*********************
 * @author 김정래
 * 작성일 : 2019-07-08
 * POST 질문등록시 카테고리/버드텐더 추천 관련 로직
 *********************/
import models, { Sequelize } from '../../../models'
import makeuuid from '../../../utils/makeuuid'
import sequelize from 'sequelize';
import jwtverify from '../../../utils/jwtverify'
const Op = Sequelize.Op;
const con = models.sequelize;


/***
 * TODO : 카테고리 받을시 그 카테고리와 관련된 추천 버드텐더 목록 띄우기.
 * 프로필 사진과 함께 보내야함.
 *  입력받은 갯수만큼 키워드를 받아서 쿼리검색.
 */

exports.getRelatedBudtenders= (req, res, next) => {
    let arr;
  
    let host = process.env.PROFILE_ADDR;
    const category = req.headers['category'];

    try{
    
        if (typeof (category) != 'object') //Json STring인 경우
            arr = JSON.parse(category);
        else
            arr = category;

         let str = 
        `SELECT 
            m.id, m.mem_email, m.mem_nickname, m.mem_uuid, m.mem_country, m.mem_state, m.mem_birth, m.mem_info,
            CONCAT('${host}',CONCAT(f.fiDestination,f.fiFilename)) url

            FROM members m 
            LEFT JOIN 
                files f ON m.mem_profile = f.fiUuid

            WHERE 
                mem_type=1 
            AND 
                mem_interested LIKE `;


        if(arr instanceof Array) {
            for(const c of arr){
                if(c.id == undefined) throw new Error("카테고리 id값이 존재하지 않습니다.");               
                str+=` '%${c.id}%' OR ` 
            }
            if(category.length>0) str = str.substr(0,str.length-3);
        }
        else if(arr.id == undefined) throw new Error("카테고리 id값이 존재하지 않습니다.");
        else str+=` '%${arr.id}%'` 
     
       
        con.query(str)
        .then(([result, metadata]) => {
            res.json({result:true,budtenders:result});
        })
        .catch(err=>{
            res.json({result:false,message:"Server error"+err});
        });  
    }catch(err){
        res.json({result:false,message:"Server error:"+err.message});
    }
}


/****
 * 버드텐더에게 알림보내기
 * 버드텐더 수만큼 알림 log를 쌓음.(bulkCreate)
 */

exports.toBudtenders= (req, res, next) => {
    const token = req.headers['x-access-token'];
    let arr;
    let body = req.body;
    let budtender = body.budtender;
    var ip = req.headers['x-forwarded-for'] || req.connection.remoteAddress;
    
    try{
        const payload = jwtverify.verify(token); 


        if (typeof (budtender) != 'object') //Json STring인 경우
            arr = JSON.parse(budtender);
        else
            arr = budtender;

        
        

        //삽입할 로그 갯수만큼 array 만듦.
         let logArray = new Array();
         for(const c of arr){
            let temp = new Object();
            temp.logType = 20;
            temp.userUuid = payload.id;
            temp.tableKey = body.postUuid;
            temp.linkId = body.postUuid;
            temp.userIp = ip;
            temp.receiveId = c.mem_uuid;
            if(temp.receiveId == undefined || body.postUuid ==undefined) throw new Error("파라미터 값이 유효하지 않습니다."); 
            logArray.push(temp);
        }

        models.log.bulkCreate(logArray)
        .then(result => {
            res.json({result:true,message:"success"});
        })
        .catch(err=>{
            res.json({result:false,message:"Server error"+err});
        }) 

    }catch(err){
        res.json({result:false,message:"Server Error:"+err.message});
    }
}


