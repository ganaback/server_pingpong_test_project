/*********************
 * @author 김정래
 * 작성일 : 2019-06-12
 * POST 질문/답변 전용 controller
 *********************/
import models, { Sequelize } from '../../../models'
import sequelize from 'sequelize';
import jwtverify from '../../../utils/jwtverify'
import { promises } from 'fs';

const con = models.sequelize;
const Op = Sequelize.Op;

 
/********
 * TODO 게시글 상세 불러오기
 * 1. 게시글 검색 : (파라미터, 존재여부 검사 )
 * 2. 게시글이 존재한다면 게시글 카테고리 가져오기
 * 3. 카테고리를 가져온후 다음의 Promises 수행
 *  3-1 . 조회수 증가 ViewCount +1 (필수가 아니므로 return 하지 않음.)
 *  3-2.  게시글 답변에 햬땅하는 Votes 내역 Object 가져오기
 * 4. 필수작업 수행후 json Object Response
 */


exports.postsDetail = (req, res, next) => {
    const postUuid = req.params.postUuid;

      try{
        if(postUuid == undefined) throw new Error("필수 파라미터가 유효하지 않습니다.");

        var resultjson;
        var votes;
        var t;
        let host = process.env.PROFILE_ADDR;

       
        //POST OBJECT 가져오기
        let pstr = `
                
            SELECT
                p.*,
                m.id,m.mem_type,m.mem_email,m.mem_nickname,m.mem_uuid,m.mem_country,m.mem_state,m.mem_birth,m.mem_info,
                CONCAT('${host}',CONCAT(f.fiDestination,f.fiFilename)) url

            FROM posts p 
                LEFT JOIN members m ON p.ownerUserId = m.mem_uuid
                LEFT JOIN files f ON m.mem_profile = f.fiUuid
            
            WHERE 
                p.postUuid ='${postUuid}'
                OR 
                p.parentId = '${postUuid}'
            
        
        `;


        //게시글에 해당하는 votes 가져오기.
        let qstr = `
        SELECT a.id answerid ,v.* FROM posts a 
            INNER JOIN votes v 
                ON a.postUuid = v.postUuid 
            WHERE a.parentId = "${postUuid}"
            ORDER BY createdAt desc;
        `;


        let category = null;
        var pr1 = con.query(pstr)
        .then(([result, metadata]) => {
            //질문과 관련된 카테고리 리스트 가져오기.
        
            if(result.length==0) {
               throw new Error('404');
            }
            resultjson = postFormatter(result);
            let cate = resultjson[0].category
            if(cate) {
                let catearr = cate.split(',');
                return models.category.findAll({where:{id:catearr}});
            } 
        })
        .then(cateResult=>{
            category = cateResult;
            models.post.update({
                viewCount: sequelize.literal('viewCount + 1')},
                { where: { postUuid: postUuid } }).then(_ => {
                  console.log('viewCount+1');
                })
            
            return con.query(qstr);
        })
        .then(([result, metadata]) => {
            votes = result;
            res.json({ result: true, posts: resultjson,category:category,votes:votes});
        })
        .catch(err=>{
            if(err.message=='404') return res.status(404).json({result:false,message:"404 : Cannot Found"});
            res.json({result:false,message:"Server error"+err});
        }) 

    }catch(err){
        res.json({result:false,message:"Server error"+err});
    }
}

/************
 * 상세 페이지 질문/답변 분리 formatter
 * 답변인 경우 질문의 answers 속성에 할당
 * 
 */

function postFormatter(posts){
    let questions = [];
    let answers =[];

    for(var i=0; i<posts.length; i++){
        if(posts[i].parentId!=undefined)
             answers.push(posts[i]);
        else
            questions.push(posts[i]);
    }

    for(var i=0; i<questions.length; i++){
        let qanswer = [];
       for(var j=0; j<answers.length; j++){
           if(questions[i].postUuid==answers[j].parentId){
                qanswer.push(answers[j]);
           }
       }
       Object.assign(questions[i],{answers:qanswer})
    }
    return questions;
}


/************
 * 질문리스트들의  질문/답변 분리 formatter
 * 답변인 경우 질문의 answers 속성에 할당
 *
 * ****** */

function allFormatter(posts){
    let questions = [];
    let answers =[];

    for(var i=0; i<posts.length; i++){
        if(posts[i].parentId!=undefined)
             answers.push(posts[i]);
        else
            questions.push(posts[i]);
    }

    for(var i=0; i<questions.length; i++){
        let qanswer = [];
       for(var j=0; j<answers.length; j++){
           if(questions[i].postUuid==answers[j].parentId){
                qanswer.push(answers[j]);
           }
       }
       Object.assign(questions[i],{answers:qanswer})
    }
    return questions;
}


//질문글만 
exports.searchQuestion = (req, res, next) => {
    const query = req.query;
    let keyword = query.keyword;
    let index = query.index;

    if(typeof index == "undefined") index = 0;
      try{
        models.post.findAndCountAll({
            where:{
                title: {
                    [Op.like]: "%" + keyword + "%"
                },
                parentId:null
            },
            order: [['createdAt', 'DESC']],
            offset:Number(index),
            limit:10
        })
        .then(posts=>{
            res.json({ result: true, count:posts.count, posts: posts.rows});
        })
        .catch(err=>{
            res.json({result:false,message:"Server error"+err});
        })
    }catch(err){
        res.json({result:false,message:"Server error"+err});
    }
}


/**
 * 전체 검색 
 * 1. 해당 키워드에 해당하는 게시물의 갯수를 구한뒤 
 * 2. 게시물 목록을 불러온다.
 * 
 */
exports.searchAll = (req, res, next) => {
    const query = req.query;
    let keyword = "%"+query.keyword+"%";
    let index = Number(query.index);

      try{


         const countstr = `
         select count(*) count from view_question WHERE title LIKE '${keyword}'
         `
 
         const str = `
         (select * from view_question WHERE title LIKE '${keyword}' ORDER BY id DESC limit ${index},10)

         UNION ALL
         
         SELECT a.*
         FROM (select * from view_question WHERE title LIKE '${keyword}' ORDER BY id DESC limit ${index},10)q
         INNER JOIN view_answer a ON q.postUuid = a.parentId 
         `


        let postall;
        let count;
        var pr1 = con.query(countstr, { replacements: ['active'], type: sequelize.QueryTypes.SELECT })
            .then(([result, metadata]) => {
                count = result;
                return con.query(str)
            })
            .then(([result, metadata]) => {
                postall = allFormatter(result);
                res.json({result:true,count:count.count,posts:postall});
            })
            .catch(err => {
                res.json({result:false,message:"error"+err.message});
            }) 


    }catch(err){
        res.json({result:false,message:"Server error"+err});
    }
}


//내 질문글 갯수

exports.myQuestionsCount = (req, res, next) => {
    const token = req.headers['x-access-token'];
    try{
        const payload = jwtverify.verify(token); 
        models.post.count({
            where:{
                ownerUserId:payload.id,
                parentId:null
            }
        })
        .then(result=>{
            res.json({ result: true, count:result});
        })
        .catch(err=>{
            res.json({result:false,message:"Server error"+err});
        })
    }catch(err){
        res.json({result:false,message:err.message});
    }
}

/***
 * 내가 쓴 질문들
 * TODO : index로부터 10개까지의 데이터를 불러온다. 
 * 답변의 경우 최신순/채택순 으로 한개의 데이터만 가져온다.
 */
exports.myQuestions = (req, res, next) => {
    console.log('dd');
    try{
        const token = req.headers['x-access-token'];
        let page = Number(req.query.index);

        if (Number.isNaN(page)) throw new Error('400');
        if(page==0) page=1;
        const index = page * 10 - 10;

        const payload = jwtverify.verify(token); 
        const str = `
        (SELECT * FROM view_question WHERE ownerUserId='${payload.id}' ORDER BY id DESC LIMIT ${index},10)

        UNION ALL
        
        SELECT a.* FROM (SELECT * FROM view_question WHERE ownerUserId='${payload.id}' ORDER BY id DESC LIMIT ${index},10)q
        INNER JOIN view_answer a ON q.postUuid = a.parentId ORDER BY acceptedAnswerId DESC
        `

        var postall;
        con.query(str)
        .then(([result, metadata]) => {
           postall = myQuestionFormatter(result);
           res.json({result:true,posts:postall});
        })
        .catch(err=>{
            if(err.message=='400') return res.status(400).json({result:false,message:"Parameter is NaN"});
            res.json({result:false,message:"Server error"+err});
        });

  }catch(err){
      res.json({result:false,message:err.message});
  }
}

//답변 1개까지만 불러오게 만듦.
function myQuestionFormatter(posts){
    let questions = [];
    let answers =[];

    for(var i=0; i<posts.length; i++){
        if(posts[i].parentId!=undefined)
             answers.push(posts[i]);
        else
            questions.push(posts[i]);
    }

    for(var i=0; i<questions.length; i++){
        let qanswer = [];
       for(var j=0; j<answers.length; j++){
           if(questions[i].postUuid==answers[j].parentId){
                qanswer.push(answers[j]);
                break;
           }
       }
       Object.assign(questions[i],{answers:qanswer[0]})
    }
    return questions;
}


/********
 * 
 * TODO : 내 답변목록의 갯수를 반환한다.
 * 
 */

exports.myAnswersCount = (req, res, next) => {
    const token = req.headers['x-access-token'];
    try{
        const payload = jwtverify.verify(token); 
        models.post.count({
            where:{
                ownerUserId:payload.id,
                parentId:{ [Op.ne]: null}
            }
        })
        .then(result=>{
            res.json({ result: true, count:result});
        })
        .catch(err=>{
            res.json({result:false,message:"Server error"+err});
        })
    }catch(err){
        res.json({result:false,message:err.message});
    }
}



//내가쓴 답변 목록 formartter
function myAnswerFormatter(posts){
    let questions = [];
    let answers =[];

    for(var i=0; i<posts.length; i++){
        if(posts[i].parentId!=undefined)
             answers.push(posts[i]);
        else
            questions.push(posts[i]);
    }

    for(var i=0; i<answers.length; i++){
        let temp = [];
       for(var j=0; j<questions.length; j++){
           if(answers[i].parentId==questions[j].postUuid){
                temp.push(questions[j]);
               // break;
           }
       }
       //result.push(temp);
       Object.assign(answers[i],{question:temp[0]})
    }
    return answers;
}

/**
 * 내가쓴 답변 목록 
 * TODO: 답변과 해당하는 질문글 함께 불러오기.
 */

exports.myAnswers = (req, res, next) => {
 
    try{
        const token = req.headers['x-access-token'];
        let page = Number(req.query.index);
        if (Number.isNaN(page)) return res.status(400).json({result:false,message:"Parameter is NaN"});
        if(page==0) page =1;
        const index = page*10-10;

        const payload = jwtverify.verify(token); 
        const str = `
        (SELECT * FROM view_answer WHERE ownerUserId='${payload.id}' ORDER BY id DESC LIMIT ${index},10)

        UNION ALL
        
        SELECT q.* FROM (SELECT * FROM view_answer WHERE ownerUserId='${payload.id}' ORDER BY id DESC LIMIT ${index},10)a
        INNER JOIN view_question q ON q.postUuid = a.parentId ORDER BY acceptedAnswerId DESC 
        `

        var postall;
        con.query(str)
        .then(([result, metadata]) => {
           postall = myAnswerFormatter(result);
           res.json({result:true,posts:postall});
        })
        .catch(err=>{
            res.json({result:false,message:"Server error"+err});
        });

  }catch(err){
      res.json({result:false,message:err.message});
  }
}



/****
 * TODO : 카테고리에 해당하는 게시글 불러오기.
 * 
 */
exports.categoryPosts = (req, res, next) => {
    const query = req.query;
    let id = query.id;
    let index = Number(query.index);

      try{
         var count;
         const countstr = `
         select count(*) count from view_question WHERE FIND_IN_SET('${id}', category)
         `
 
         const str = `
         (select * from view_question WHERE FIND_IN_SET('${id}', category) ORDER BY id DESC limit ${index},10)

         UNION ALL
         
         SELECT a.*
         FROM (select * from view_question WHERE FIND_IN_SET('${id}', category) ORDER BY id DESC limit ${index},10)q
         INNER JOIN view_answer a ON q.postUuid = a.parentId 
         `


        var pr1 = con.query(countstr,{ replacements: ['active'], type: sequelize.QueryTypes.SELECT })
        .then(([result, metadata]) => {
            count = result;
        })
        .catch(err=>{
            res.json({result:false,message:"Server error"+err});
        }) 

        var postall;
        var pr2 = con.query(str)
        .then(([result, metadata]) => {
           postall = allFormatter(result);
        })
        .catch(err=>{
            res.json({result:false,message:"Server error"+err});
        });

        Promise.all([pr1,pr2]).then(()=>{
            res.json({result:true,count:count.count,posts:postall});
        })
       

    }catch(err){
        res.json({result:false,message:"Server error"+err});
    }
}

