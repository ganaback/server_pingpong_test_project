/*********************
 * @author 김정래
 * 작성일 : 2019-06-12
 * POST 질문/답변 전용 controller
 *********************/
import models, { Sequelize } from '../../../models'
import makeuuid from '../../../utils/makeuuid'
import sequelize from 'sequelize';
import jwtverify from '../../../utils/jwtverify'
import { strict } from 'assert';
const Op = Sequelize.Op;


/***
 * 알림기능관련 로그 생성
 * 
 * 
 */
function createLog(type,obj,ip,receiveId){

    models.log.create({
        logType:type,
        tableKey:obj.postUuid,
        linkId:obj.parentId,
        userUuid:obj.ownerUserId,
        userIp:ip,
        category:obj.category,
        receiveId:receiveId
    }).then(result=>{
        console.log("create log");
    })  

}

/*******
 * 질문글 생성
 * category는 json String 이므로 array로 파싱
 * 질문글이 생성된경우 로그를 남긴다.
 * 
 * ********* */
exports.create = (req, res, next) => {
   const body = req.body;
   const token = req.headers['x-access-token'];
   var ip = req.headers['x-forwarded-for'] || req.connection.remoteAddress;
   let resultObj;
   let receiveId = body.receiveId

    try{
        let category = body.category;

        if(body.title == undefined) throw new Error("제목을 입력하지 않았습니다.");
        else if(body.title.length>255) throw new Error("제목의 글자수가 초과되었습니다. (255 bytes)");
       

        let arr;
        if (typeof (category) != 'object') //Json STring인 경우
            arr = JSON.parse(category);
        else
            arr = category;

        let categoryStr = "";

        for (const param of arr) {
            categoryStr += param.id + ",";
        }

        const payload = jwtverify.verify(token);
        models.post.create({
            title:body.title,
            postUuid:makeuuid.makeuuid(),
            ownerUserId:payload.id,
            ownerUserName:payload.nickname,
            category:categoryStr.substr(0, categoryStr.length -1)
        })
        .then(result=>{
            res.json({ result: true, message: "create success",postUuid:result.postUuid,category:category});
        })  
        .catch(err=>{
            res.json({result:false,message:err.message});
        });  

        //res.json({result:'test 중 '});
 
        //로그남기기
         
     
    }
    catch(err){
        console.log(err);
        res.json({result:false,message:err.message});
    }
}


/** 
 * 답변글 생성
 * 
 * 트랜잭션을 사용하여 답변글 생성후 , 답변수를 update 시킨다. 에러일 경우 Rollback.
 * 모든 작업이 수행된 이후 , 로그삽입
 * 
 * **/
exports.createAnswer = (req, res, next) => {
    const body = req.body;
    const token = req.headers['x-access-token'];
    var ip = req.headers['x-forwarded-for'] || req.connection.remoteAddress;
    let receiveId = body.receiveId

     try{
        if(body.body == undefined) throw new Error("본문을 입력하지 않았습니다.");
        else if(body.body.length>3000) throw new Error("본문의 글자수가 초과되었습니다. (3000 bytes)");
        else if(body.parentId==undefined || body.parentId == "") throw new Error("질문글의 ID값이 비었습니다.");
        const payload = jwtverify.verify(token);
        console.log(payload);

        if(payload.answer == 0 ) throw new Error('버드텐더인 사람만 답변할 수 있습니다.');


         var resultjson;
         var t;
        //답변을 달았을떄 답변수가 카운트되는 트랜잭션 작성.
        models.sequelize.transaction().then(function(transaction){
            t = transaction;
            return models.post.findAll({
               where:{
                   postUuid:body.parentId,
                   parentId:null
               }
            },{transaction:t});
        }).then(result=>{
            if(!result || result.length ==0){
                throw new Error('Question not Found');
            } 
            else return models.post.create({
                body:body.body,
                postUuid:makeuuid.makeuuid(),
                ownerUserId:payload.id,
                ownerUserName:payload.nickname,
                parentId:body.parentId
            },{transaction:t});
        }).then(result=>{
            resultjson=result;
            return models.post.update({
                answerCount: sequelize.literal('answerCount + 1')},
             {where:{postUuid:body.parentId},transaction:t});
        }) //then end
        .then(()=>{
            return t.commit();
            }
        ).then(()=>{
            createLog(11,resultjson,ip,receiveId);
            res.json({ result: true, message: "create Answer success"});
        })
        .catch((err)=>{
            if(t) t.rollback();
            if(err.message=='Question not Found') {
                console.log('404 test');
                return res.status(404).json({ result: false, message:err.message});
            }
            next(err);
        });
    
     }catch(err){
         res.json({result:false,message:err.message});
     }
 }
 



/**
 * TODO: 게시물 수정
 * 게시물 수정이 완료된 후에는 게시물수정 로그를 남긴다.
 * 
 */
exports.postsUpdate = (req, res, next) => {
    const token = req.headers['x-access-token'];
    const body = req.body;
    var ip = req.headers['x-forwarded-for'] || req.connection.remoteAddress;
    var type;
    if(req.url == "/update") type=10;
    else type = 12;

    var ip = req.headers['x-forwarded-for'] || req.connection.remoteAddress;

      try{
        if(body.body == undefined && body.title== undefined) throw new Error("본문 또는 제목을 입력하지 않았습니다.");
        else if(body.body.length>3000 || body.title.length>255) throw new Error(" 글자수가 초과되었습니다.");


        const payload = jwtverify.verify(token);

        let param ={
            body:body.body,
            title:body.title}
         
        models.post.update(
            param,
            {where:{postUuid:body.postUuid}}
        )
        .then(posts=>{
            res.json({ result: true, message:"Update Succeed"});
        })
        .catch(err=>{
            res.json({result:false,message:err.message});
        });  
    }catch(err){
        res.json({result:false,message:"Error:: "+err});
    }
}

/** 
 * 답변채택
 * 
 * 질문자가 답변을 채택했는지의 여부를 확인한 후 
 * 이미 채택했을 경우 error 날리기
 * 아직 채택을 안한경우 채택 로직 (update) 수행
 * 
 * **/
exports.accepted = (req, res, next) => {
    const token = req.headers['x-access-token'];
    const body = req.body;
    var ip = req.headers['x-forwarded-for'] || req.connection.remoteAddress;
    let receiveId = body.receiveId
    

      try{
        if(body.answerUuid == undefined || body.postUuid == undefined) throw new Error("필수 파라미터가 유효하지 않습니다.");
        const payload = jwtverify.verify(token);
        let param ={
            ownerUserId:payload.id,
            postUuid:body.answerUuid,
            parentId:body.postUuid
        }
        models.post.findOne({where:{postUuid:body.postUuid,acceptedAnswerId:{[Op.ne]:null}}})
        .then(obj=>{
            if(obj) 
            { 
                throw new Error("already accepted");
            }
           else {
                return models.post.update(
                    {
                        acceptedAnswerId:body.answerUuid
                    },
                    {where:{postUuid:body.postUuid}}
                )
           }
        })
        .then(()=>{
            createLog(16,param,ip,receiveId);
            res.json({ result: true, message:"Update Succeed"});
        })
        .catch(err=>{
            res.json({result:false,message:err.message});
        });  

    }catch(err){
        res.json({result:false,message:"Error:: "+err});
    }
}

/******
 * TODO : 텍스트 에디터 이미지 파일 업로드용
 * 업로드가 완료된 경우 url을 결과값으로 return 
 */
exports.imgUpload =(req,res,next)=>{
    try{
        const files = req.files;
        const path = process.env.HOST_ADDR+"/postImg/"
        if(!files) throw new Error("파일이 존재하지 않습니다.");
        else if(req.fileValidationError) throw new Error(req.fileValidationError);
        else{
            res.json({result:true,url:path+files[0].filename});
        }
    }catch(err){
        res.json({result:false,message:"Error:: "+err});
    }
}