const request = require('supertest');
const should = require('should');
const app = require('../../../app');

/**.
 * 게시글관련 테스트 코드 작성
 */



 /**
  * 1. 글 상세보기 테스트 
  * 
  */
describe('GET /posts/detail/:postUuid는',()=>{
    const postUuid = "44be5cae78116af18d4d3d13027769f9";
    describe('성공시',()=>{
        it('게시글의 상세정보를 표시한다.',(done)=>{
            request(app)
            .get(`/posts/detail/${postUuid}`)
            .expect(200)
            .end((err,res)=>{
                res.body.should.be.instanceOf(Object);
                done();
            })
        })
    })

    describe('실패시',()=>{
        it('post번호로 게시글을 찾을 수 없을 경우 404로 응답한다.',(done)=>{
            request(app)
            .get('/posts/detail/wedd')
            .expect(404)
            .end(done);
        })
    })
})




 /**
  * 2. 내 질문글 보기 테스트 
  * 
  */
 describe('GET /myQuestions는',()=>{
    const token = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJlbWFpbCI6Im1zajkxMjFAZ21haWwuY29tIiwibmlja25hbWUiOiJTdW5nIEppbiwgbW9vbiIsImlkIjoiNDFiNmI5NGVjMjM3OGRlYzhlNmI5NDdkY2I3ZGY4MDkiLCJhbnN3ZXIiOjAsImlhdCI6MTU2MjkxOTQ0NywiZXhwIjoxNTY4MTAzNDQ3fQ.5AjWtcHqc_2MM1UPaHyFadB8Ep2MuIMpAJxT1c5cNxk';
    describe('성공시',(done)=>{
        it('내가쓴 게시물 오브젝트를 반환한다.',()=>{
            request(app)
            .get('/myQuestions?index=1')
            .set('x-access-token',token)
            .expect(200)
            .end((err,res)=>{ 
                res.body.should.be.instanceOf(Object);
                done();
            });
        })
    })

    describe('실패시',(done)=>{
        it('index 파라미터가 누락된 경우 400을 반환한다.',()=>{
            request(app)
            .get('/myQuestions')
            .set('x-access-token',token)
            .expect(400)
            .end(done);
        })

        it('index가 숫자값이 아닌 경우 400을 반환한다',()=>{
            request(app)
            .get('/myQuestions?index=ww')
            .set('x-access-token',token)
            .expect(400)
            .end(done);
        })
    })
})


 /**
  * 3. 내 답변글 보기 테스트 
  * 
  */
 describe('GET /myAnswers',()=>{
    const token = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJlbWFpbCI6Im1zajkxMjFAZ21haWwuY29tIiwibmlja25hbWUiOiJTdW5nIEppbiwgbW9vbiIsImlkIjoiNDFiNmI5NGVjMjM3OGRlYzhlNmI5NDdkY2I3ZGY4MDkiLCJhbnN3ZXIiOjAsImlhdCI6MTU2MjkxOTQ0NywiZXhwIjoxNTY4MTAzNDQ3fQ.5AjWtcHqc_2MM1UPaHyFadB8Ep2MuIMpAJxT1c5cNxk';
    describe('성공시',(done)=>{
        it('내가쓴 답변글 오브젝트를 반환한다.',()=>{
            request(app)
            .get('/myAnswers?index=1')
            .set('x-access-token',token)
            .expect(200)
            .end((err,res)=>{ 
                res.body.should.be.instanceOf(Object);
                done();
            });
        })
    })

    describe('실패시',(done)=>{
        it('index 파라미터가 누락된 경우 400을 반환한다.',()=>{
            request(app)
            .get('/myAnswers')
            .set('x-access-token',token)
            .expect(400)
            .end(done);
        })

        it('index가 숫자값이 아닌 경우 400을 반환한다',()=>{
            request(app)
            .get('/myAnswers?index=ww')
            .set('x-access-token',token)
            .expect(400)
            .end(done);
        })
    })
})

/*4. 질문검색기능.*/

describe('GET /posts/searchQuestion 은',()=>{
    describe('성공시',(done)=>{
        it('키워드에 해당하는 질문글 리스트를 반환한다',()=>{
            request(app)
            .get('/posts/searchQuestion?keyword=dd&index=1')
            .expect(200)
            .end((err,res)=>{ 
                res.body.should.be.instanceOf(Object);
                res.body.should.have.property('parentId',null);
                done();
            });
        })

        it('키워드에 공백이 들어가도, 질문글 리스트를 반환한다',()=>{
            request(app)
            .get('/posts/searchQuestion?keyword=&index=1')
            .expect(200)
            .end((err,res)=>{ 
                res.body.should.be.instanceOf(Object);
                res.body.should.have.property('parentId',null);
                done();
            });
        })
    })

     describe('실패시',(done)=>{
        it('index 파라미터가 누락된 경우 400을 반환한다.',()=>{
            request(app)
            .get('/posts/searchQuestion?keyword=dd')
            .expect(400)
            .end(done);
        })

        it('index가 숫자값이 아닌 경우 400을 반환한다',()=>{
            request(app)
            .get('/posts/searchQuestion?keyword=dd&index=www')
            .expect(400)
            .end(done);
        })

        it('index가 0보다 작은 값일 경우 400을 반환한다',()=>{
            request(app)
            .get('/posts/searchQuestion?keyword=dd&index=-1')
            .expect(400)
            .end(done);
        })
    }) 
})



/*4. 질문검색기능.*/

describe('GET /posts/searchDetail 은',()=>{
    describe('성공시',(done)=>{
        it('키워드에 해당하는 질문글과 답변글 리스트를 반환한다',()=>{
            request(app)
            .get('/posts/searchDetail?keyword=dd&index=1')
            .expect(200)
            .end((err,res)=>{ 
                res.body.should.be.instanceOf(Object);
                res.body.should.have.property('answers'); //답변리스트가 속성으로 있어야함.
                done();
            });
        })

        it('키워드에 공백이 들어가도, 질문글 리스트를 반환한다',()=>{
            request(app)
            .get('/posts/searchDetail?keyword=&index=1')
            .expect(200)
            .end((err,res)=>{ 
                res.body.should.be.instanceOf(Object);
                res.body.should.have.property('answers');
                done();
            });
        })
    })

     describe('실패시',(done)=>{
        it('index 파라미터가 누락된 경우 400을 반환한다.',()=>{
            request(app)
            .get('/posts/searchDetail?keyword=dd')
            .expect(400)
            .end(done);
        })

        it('index가 숫자값이 아닌 경우 400을 반환한다',()=>{
            request(app)
            .get('/posts/searchDetail?keyword=dd&index=www')
            .expect(400)
            .end(done);
        })

        it('index가 0보다 작은 값일 경우 400을 반환한다',()=>{
            request(app)
            .get('/posts/searchDetail?keyword=dd&index=-1')
            .expect(400)
            .end(done);
        })
    }) 
})






/**
 * 7. 답변글 작성 테스트
 */
describe('POST /posts/createAnswer 는',()=>{
    const token = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJlbWFpbCI6InRlc3RAbmF2ZXIuY29tIiwibmlja25hbWUiOiJ1dSIsImlkIjoiNDc3NmQxYWZkNjIzYzlkYWFmMzQyM2E2ZDYwODA3ZDgiLCJhbnN3ZXIiOjEsImlhdCI6MTU2NDU2MDUwNSwiZXhwIjoxNTY5NzQ0NTA1fQ.E5BJs-eG2bulrTDN0231PxYZFO9gEvt9qDoxYWOm910';
    const content = "test content";
    const parentId = "4e4006ce3df4d1c4b7c96179fc1d649b";

    describe.only('성공시',(done)=>{
        it('result true 값을 반환한다.',()=>{
            request(app)
            .post('/posts/createAnswer')
            .set('x-access-token',token)
            .send({'body':content,parentId:parentId})
            .expect(201)
            .end((err,res)=>{ 
                const body = res.body;
                body.should.have.property('result',true);
                done();
            });
        })
    })

    describe.only('실패시',(done)=>{
        it('body값이 누락된 경우 상태코드 400을 반환한다.',()=>{
            request(app)
            .post('/posts/createAnswer')
            .set('x-access-token',token)
            .send({parentId:parentId})
            .expect(400)
            .end(done);
        })

        it('parentId 값이 누락된 경우 상태코드 400을 반환한다.',()=>{
            request(app)
            .post('/posts/createAnswer')
            .set('x-access-token',token)
            .send({body:content,parentId:""})
            .expect(400)
            .end(done);
        })

        it('해당 질문글 포스트를 찾지못한 경우 상태코드 404를 반환한다.',()=>{
            request(app)
            .post('/posts/createAnswer')
            .set('x-access-token',token)
            .send({parentId:'11',body:content})
            .expect(404)
            .end(done);
        })

        it('답변 작성자가 budtender가 아닌 경우 상태코드 404를 반환한다.',()=>{
            const InvalidToken = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJlbWFpbCI6Im1zajkxMjFAZ21haWwuY29tIiwibmlja25hbWUiOiJTdW5nIEppbiwgbW9vbiIsImlkIjoiNDFiNmI5NGVjMjM3OGRlYzhlNmI5NDdkY2I3ZGY4MDkiLCJhbnN3ZXIiOjAsImlhdCI6MTU2MjkxOTQ0NywiZXhwIjoxNTY4MTAzNDQ3fQ.5AjWtcHqc_2MM1UPaHyFadB8Ep2MuIMpAJxT1c5cNxk';
            request(app)
            .post('/posts/createAnswer')
            .set('x-access-token',InvalidToken)
            .send({postUuid:'11',body:content})
            .expect(404)
            .end(done);
        })
    })
})

/**
 * 질문글 작성테스트
 */
describe('POST /posts/create 는',()=>{
    const token = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJlbWFpbCI6Im1zajkxMjFAZ21haWwuY29tIiwibmlja25hbWUiOiJTdW5nIEppbiwgbW9vbiIsImlkIjoiNDFiNmI5NGVjMjM3OGRlYzhlNmI5NDdkY2I3ZGY4MDkiLCJhbnN3ZXIiOjAsImlhdCI6MTU2MjkxOTQ0NywiZXhwIjoxNTY4MTAzNDQ3fQ.5AjWtcHqc_2MM1UPaHyFadB8Ep2MuIMpAJxT1c5cNxk';
    const title = "test code";
    const category = [{"id":1,"categoryName":"politics"}];

    describe('성공시',(done)=>{
        it('result true 값을 반환한다.',()=>{
            request(app)
            .post('/users')
            .set('x-access-token',token)
            .send({title:title,category:category})
            .expect(201)
            .end((err,res)=>{ 
                const body = res.body;
                body.should.have.property('result',true);
                done();
            });
        })
    })

    describe('실패시',(done)=>{
        it('title이 누락된 경우 400을 반환한다.',()=>{
            request(app)
            .post('/users')
            .send({})
            .expect(400)
            .end(done);
        })
    })
})



