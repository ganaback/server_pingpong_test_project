/*********************
 * @author 김정래
 * 작성일 : 2019-06-12
 * POST 질문/답변 전용 router
 *********************/

import controller from './post.controller'
import update from './update.controller'
import related from './related.controller'
import fileupload from '../../../utils/fileupload'
const upload = fileupload.postImg();

module.exports = function(app){//함수로 만들어 객체 app을 전달받음
	var express = require('express');
    var router = express.Router();

    /*** 
     * 조회 기능들
     * **/ 
    router.get('/detail/:postUuid',controller.postsDetail);

    router.get('/myQuestions',controller.myQuestions);
    router.get('/myQuestionsCount',controller.myQuestionsCount);
    router.get('/myAnswers',controller.myAnswers);
    router.get('/myAnswersCount',controller.myAnswersCount);
    router.get('/searchQuestion',controller.searchQuestion);
    router.get('/searchDetail',controller.searchAll);


    /* create  및 update  */
    router.post('/create',update.create);   
    router.post('/createAnswer',update.createAnswer);
    router.post('/update',update.postsUpdate);
    router.post('/updateAnswer',update.postsUpdate);
    router.post('/accept',update.accepted);

    router.get('/budtenders',related.getRelatedBudtenders);
    router.post('/tobudtenders',related.toBudtenders);

    /**카테고리 및 추천 관련 */
    router.get('/category',controller.categoryPosts);

    //이미지 업로드
   // router.get('/category',update.imgUpload);
   router.post('/upload',upload.array("attach",1),update.imgUpload) ;

	return router;	//라우터를 리턴
};