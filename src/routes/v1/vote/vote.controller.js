/*********************
 * @author 김정래
 * 작성일 : 2019-06-12
 * POST 질문/답변 전용 controller
 *********************/
import models, { Sequelize } from '../../../models'
import sequelize from 'sequelize';
import jwtverify from '../../../utils/jwtverify'
import jwt from 'jsonwebtoken'
const con = models.sequelize;

const Op = Sequelize.Op;


//추천기능
/**
 * 1. 먼저 사용자가 해당게시물에 추천했는지 검사 (FindOne)
 * 2. 존재하는 경우 업데이트를 실행하고 존재하지 않는경우 vote 테이블에 insert
 * 3. 단, 중복추천인경우 update허용하지 않음.
 * 4. Vote 테이블을 업데이트하고나서, 게시글의 추천,비추천 카운트를 업데이트한다.
 * 5. 테이블이 업데이트되고 난 이후의 답변의 카운트를 조회한다.
 * 7. 작업이 다 끝나면 Transaction Commit
 * 8. 로그삽입은 병렬로 실행.
 */


exports.vote = (req, res, next) => {
    var ip = req.headers['x-forwarded-for'] || req.connection.remoteAddress;
    const token = req.headers['x-access-token'];
    const body = req.body;
    var userUuid;
   
    try {

        const payload = jwtverify.verify(token);

        var literal;
        var count;
        var t;
        if (Number(body.voteType) == 1) literal = 'voteCount+1';
        else literal = 'voteCount-1';
        var inputVote;

        let contents = (Number(body.voteType)==1)?"upvote":"downvote";

        let logParam = {
            logType:13,
            tableKey:body.postUuid,
            userUuid:payload.id,
            contents:contents,
            receiveId:body.receiveId,
            userIp:ip
        }
    
        var pr1 = models.sequelize.transaction().then(function (transaction) {
            t = transaction;
            return models.vote.findOne({ where: { userUuid: payload.id, postUuid: body.postUuid }, transaction: t });
        })
        .then((obj) => {
            if (obj) {

                //취소인경우
                if (obj.dataValues.voteType != 0 && (obj.dataValues.voteType != body.voteType)) inputVote = 0;
                else if (obj.dataValues.voteType == body.voteType) { //중복추천인 경우.
                    throw new Error("already voted");
                }
                else inputVote = body.voteType
                return obj.update({ voteType: inputVote });
            }
            else {
                return models.vote.create({
                    voteType: body.voteType,
                    postUuid: body.postUuid,
                    userUuid: payload.id
                });
            }
        })
        .then(() => {
            return models.post.update({
                voteCount: sequelize.literal(literal)
            },
                { where: { postUuid: body.postUuid }, transaction: t });
        })
        .then(() => {
            return models.post.findAll({
                where: { postUuid: body.postUuid }, transaction: t
            });
        })
        .then(result => {
            count = result;
            //logParam.linkId = count[0].dataValues.parentId;
            Object.assign(logParam,{linkId:count[0].dataValues.parentId})
            console.log(logParam.linkId);
            return t.commit();
        })
        .then(() => {
            res.json({ result: true, count: count[0].dataValues.voteCount });
        })
        .then(() => {
            return models.log.create(logParam);
        })
        .catch((err) => {
            if (t) t.rollback();
            res.json({ result: false, message: err.message })
        });



    } catch (err) {
        res.json({ result: false, message: err.message });
    }
}


/***
 * 미사용 기능
 * 
 * * */
exports.userVotes = (req, res, next) => {
    const token = req.headers['x-access-token'];
    const body = req.body;

    try {
        const payload = jwtverify.verify(token);

        var countstr = `
        select (select count(*) from votes where userUuid = "${payload.id}" and postUuid = "${body.postUuid}"
       and voteType=1) upvote, count(*) downvote from votes where userUuid = "${payload.id}" and postUuid = "${body.postUuid}"
       and voteType=2;    
        `;
        var count;
        con.query(countstr, { replacements: ['active'], type: sequelize.QueryTypes.SELECT })
            .then(([result, metadata]) => {
                count = result;
                res.json({ result: true, voteCount: count });
            })
            .catch(err => {
                res.json({ result: false, message: "Server error" + err });
            })

    } catch (err) {
        res.json({ result: false, message: "Server error" + err });
    }
}
