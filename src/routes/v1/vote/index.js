/*********************
 * @author 김정래
 * 작성일 : 2019-06-18
 * 추천/비추천 전용 router
 *********************/

import controller from './vote.controller'

module.exports = function(app){//함수로 만들어 객체 app을 전달받음
	var express = require('express');
    var router = express.Router();

    /*** 
     * 조회 기능들
     * **/ 
    router.post('/',controller.vote) ;
    router.post('/userVotes',controller.userVotes) ;

	return router;	//라우터를 리턴
};