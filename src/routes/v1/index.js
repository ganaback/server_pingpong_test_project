module.exports = function(app){//함수로 만들어 객체 app을 전달받음
	var express = require('express');
	var router = express.Router();




	var member = require('./member')(app);
	app.use("/users", member);

	var category = require('./category')(app);
	app.use("/category", category);

	var file = require('./file')(app);
	app.use("/file", file);

	app.use("/budtender", require('./budtender')(app));

	app.use("/article" , require('./article')(app));
	app.use("/support", require('./support')(app));
	app.use("/posts", require('./post')(app));
	app.use("/votes", require('./vote')(app));
	app.use("/inbox", require('./inbox')(app));





	return router;	//라우터를 리턴
};