const uuid4 = require("uuid4");
const nodemailer = require("nodemailer");
const crypto = require("crypto");
let jwt = require("jsonwebtoken");
let secretObj = require("../../config/jwt");
import jwtverify from '../../utils/jwtverify';
const models = require("../../models");``

var util = {};

const transporter = nodemailer.createTransport({
  service: "gmail",
  auth: {
    user: "leegm@gananetworks.com", // gmail 계정 아이디를 입력
    pass: "ganayo89" // gmail 계정의 비밀번호를 입력
  }
});

util.makeUuid4 = function() {
  const tokens = uuid4().split("-");
  const uuid = tokens[2] + tokens[1] + tokens[0] + tokens[3] + tokens[4];
  const uuidm = uuid.replace(/\-/g, "");
  return uuidm;
};

//이메일 회원 인증메일 발송
util.joinEmailMember = function(email, token, res, re) {
  const mailOptions = {
    from: "youremail@gmail.com",
    to: email,
    subject: "안녕하세요, gana입니다. 이메일 인증을 해주세요.",
  
    html:
      "<p>아래의 링크를 클릭해주세요 !</p>" +
      "<a href='http://192.168.0.119:3000/users/auth/?email=" +
      email +
      "&token=" +
      token +
      "'>인증하기</a>"
  };

  transporter.sendMail(mailOptions, function(err, info) {
    if (err) {
      //이메일 전송에러
      res.json({ result: false, message: "이메일 전송에러" + err });
    } else {
      res.json({ result: true, message: 3, info :"이메일 " + re +"전송 완료" });
      //chToken(email, token, res);
      //return true;
    }
  });
};

util.emailCheckPw = function(email, token, res) {
  let mailOptions = {
    from: "youremail@gmail.com",
    to: email,
    subject: "안녕하세요, gana입니다.",

    html:
      "<p>임시 비밀번호가 발급되었습니다. 임시 비밀번호를 사용하시려면 아래의 링크를 클릭해주세요 !</p>" +
      "<a href='http://192.168.0.119:3000/users/chpw/?email=" +
      email +
      "&token=" +
      token +
      "'>인증하기</a>" +
      "<br>임시 비밀번호 : " +
      token
  };

  transporter.sendMail(mailOptions, function(error, info) {
    if (error) {
      res.json({ result: false, message: "이메일 전송에러" + errror });
    } else {
      res.json({ result: true, message: info });
    }
  });
};

util.loginToken = function(loginInfo, res){
  console.log(loginInfo.budtender)
  let token = jwt.sign(
    {
      // email: body.email, // 토큰의 내용(payload)
      // nickname : result.mem_nickname,
      // uuid : uuid
      email: loginInfo.mem_email, // 토큰의 내용(payload)
      nickname : loginInfo.mem_nickname,
      id : loginInfo.mem_uuid,
      answer : loginInfo.budtender
    },
    secretObj.secret, // 비밀 키
    {
      expiresIn: "60d" // 유효 시간은 60일
    }
  );
  //TODO: 요구
  // res.cookie("user", token);  
  // req.session.ganatoken = token;
  res.json({ result: true, message: token , info : "로그인"});
}

util.loginToken2 = function(loginInfo, nickname,res){
  let token = jwt.sign(
    {
      // email: body.email, // 토큰의 내용(payload)
      // nickname : result.mem_nickname,
      // uuid : uuid
      email: loginInfo.mem_email, // 토큰의 내용(payload)
      nickname : nickname,
      id : loginInfo.mem_uuid,
      answer : loginInfo.answer
    },
    secretObj.secret, // 비밀 키
    {
      expiresIn: "60d" // 유효 시간은 60일
    }
  );
  //TODO: 요구
  // res.cookie("user", token);  
  // req.session.ganatoken = token;
  res.json({ result: true, message: token , info : "2차회원가입 완료, 로그인"});
}

util.makeHashPw = function(pw, salt){
      let hashPassword = crypto
        .createHash("sha512")
        .update(pw + salt)
        .digest("hex");
      return hashPassword;
}

util.resjsion = function(res, result, message){
  return res.json({result : true, message: result});
}

util.resjsionInfo = function(res, result, message,info){
  return res.json({result : result, message: message, info :info});
}

util.resjsionfail=function(res, message){
  return res.json({result : false, message: "fail : " + message});
}

util.resjsionerr=function(res, message, err){
  return res.json({result : false, message: message, err : err});
}

util.getId = function(token){
  const payload = jwtverify.verify(token); 
  return payload.id;
}

util.createLog =function(type,mem_uuid,tableUuid,req){
  var ip = req.headers['x-forwarded-for'] || req.connection.remoteAddress;
  var content;
  // if(type==9 || type==10) content = obj.title;
  // else content = obj.body;
  //if(type==83 || type==84) ip = manager;

  models.log.create({
      logType:type,
      tableKey:tableUuid,
      userUuid:mem_uuid,
      contents:content,
      userIp:ip,
      receiveId:mem_uuid
  })
  .then(result=>{
    console.log("log 입력 성공" + result)
  })
  .catch(err=>{
    console.log("log 입력 err" + err)
  })
}

module.exports = util;
