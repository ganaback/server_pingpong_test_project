const local = require("./localStrategy");
const facebook = require("./facebook")

module.exports = passport => {
  // passport.serializeUser((user, done) => {
  //   done(null, user.id);
  // });

  // passport.deserializeUser((id, done) => {
  //   User.find({ where: { id } })
  //     .then(user => done(null, user))
  //     .catch(err => done(err));
  // });

  passport.serializeUser((user, done) => {
    // Strategy 성공 시 호출됨
    console.log('serialize');
    done(null, user); // 여기의 user가 deserializeUser의 첫 번째 매개변수로 이동
  });

  passport.deserializeUser((user, done) => {
    // 매개변수 user는 serializeUser의 done의 인자 user를 받은 것
    console.log('deserializeUser');
    done(null, user); // 여기의 user가 req.user가 됨
  });
  
  facebook(passport);
  local(passport);
};
