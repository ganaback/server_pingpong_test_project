const LocalStrategy = require('passport-local').Strategy;

import util from "../util.js";
import msql from "../sql/member_sql";
let jwt = require("jsonwebtoken");
let secretObj = require("../../../config/jwt");

module.exports = (passport) => {
  // passport.use(new LocalStrategy({
  //   usernameField: 'email',
  //   passwordField: 'pw',
  //   session: true, // 세션에 저장 여부
  //   passReqToCallback: false,
  //   }, async (email, pw, done) => {
  //   try {
  //     console.log(email, pw)
  //     const result = await msql.findOneEmail(email)
  //     if(result){
  //       let dbPassword = result.mem_pw;
  //       const hashPassword = util.makeHashPw(pw, result.salt);

  //       if (dbPassword === hashPassword) {
  //         //로그인 처리
  //         if(result.mem_type ==0){
  //           done( null, false, { result: false, message: 1 ,info :"미인증 회원"});
  //         }else{
  //           if(!result.mem_droptime){
  //             //util.createLog(56,result.mem_uuid,null,req);
  //             let token = jwt.sign(
  //               {
  //                 // email: body.email, // 토큰의 내용(payload)
  //                 // nickname : result.mem_nickname,
  //                 // uuid : uuid
  //                 email: result.mem_email, // 토큰의 내용(payload)
  //                 nickname : result.mem_nickname,
  //                 id : result.mem_uuid,
  //                 answer : result.budtender
  //               },
  //               secretObj.secret, // 비밀 키
  //               {
  //                 expiresIn: "60d" // 유효 시간은 60일
  //               }
  //             );
  //             // util.loginToken(result, res);
  //             done(null, token, {member:result});
  //           }else{
  //             done( null, false,  {result: false, message:  2 ,info :"휴먼 회원" });
  //           }            
  //         }
  //       } else {
  //         done( null, false, { result: false, message:  3 ,info :"비밀번호 불일치" });
  //       }
  //     }else{
  //       done(null, false, { result: false, message:  4 ,info :"없는 이메일" })
  //     }
  //   } catch (error) {
  //     console.error(error);
  //     done(error);
  //   }
  // }));

const FacebookStrategy = require('passport-facebook').Strategy;
 const facebookCredentials = require('../../../config/facebook.json');
 facebookCredentials.profileFields = ['id', 'emails', 'name'];

 passport.use(new FacebookStrategy(facebookCredentials,
 function(accessToken, refreshToken, profile, done) {
    const email = profile.emails[0].value;
    const facebookId = profile.id;
    console.log(email, facebookId)
    done(null, email, facebookId)
    
    //id=uuid
    //facebookId = profile.id
    // console.log('FacebookStrategy')
    // console.log(accessToken);
    // console.log(profile);
   }
 ));

};
