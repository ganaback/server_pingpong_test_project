const express = require("express");
const router = express.Router();
const fs = require("fs");
import util from "../util.js";

const models = require("../../../models");
import filesql from "../sql/file_sql";
import msql from "../sql/member_sql";

// const Member = this.sequelize.define('member', {});
// const File = this.sequelize.define('File', {});


router.get("/tt", (req, res, next)=>{
    let test = '<input type="submit" value="파일 업로드하기">';
    // test = test.replace("<", "&lt;");
    // test = test.replace(">", "&gt;");
    res.send(test)
})


router.get("/test", (req, res, next)=>{

    models.member.findOne({
        
        where : { mem_uuid : "41d0f1c34873bb2eb1b1990403eb27c8"},
        include : [
            {model : models.file}
        ]
    })
    .then(result =>{
        res.send(result)
    })
    .catch(err =>{
        res.send(err +"")
    })
})

module.exports = router;