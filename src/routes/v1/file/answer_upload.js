const express = require("express");
const router = express.Router();
const fs = require("fs");
const multer = require("multer");
const path = require("path");
const thumb = require('node-thumbnail').thumb;
import filesql from "../sql/file_sql";
import msql from "../sql/member_sql";
import util from "../util.js";
import { promises, unwatchFile } from "fs";




let storage = multer.diskStorage({
  destination: function(req, file, callback) {
    callback(null, "certification/");
  },
  filename: function(req, file, callback) {
    let extension = path.extname(file.originalname);
    let basename = path.basename(file.originalname, extension);
    callback(null, basename + "-" + Date.now() + extension);
  }
});

let upload = multer({
  storage: storage
});

// 뷰 페이지 경로
router.get("/show2", function(req, res, next) {
  res.send("show")
});

// 프로필 사진 파일 업로드 처리
//form name = profile
router.post("/certificationupload", upload.array('certification',3), function(
  req,
  res,
  next
) {
  const files = req.files;
  const id = req.body.id;
  
  console.log(files.length);
  // if(files.length >3){
  //   res.json({result:false, message:"file은 3개를 초과할 수 없습니다."})
  // }

  //let uploadcer = 
  let result = new Array();
  for(let i =0; i<files.length; i++){
    result[i] =filesql.upload(id, null, files[i], 3)
  }
  
  let fiUuid= new Array();
  Promise.all([result[0], result[1], result[2]])
  .then(file =>{
    file.forEach(function(item, index){
      if(item){
        fiUuid[index] = item.fiUuid;
      }
    })
  })
});


const fileFilterImg = function(req, file, cb){
  let typeArray = file.mimetype.split('/');
  let fileType = typeArray[1];
  if(fileType =='jpg' ||
  fileType =='png' ||
  fileType =='jpeg' ||
  fileType =='gif' ){
    cb(null, true)
  }else{
    cb(null, false)
    req.fileValidationError = "jpg,png,jpeg,gif 파일만 업로드 가능";
  }
}

const fileFilterCer = function(req, file, cb){
  let typeArray = file.mimetype.split('/');
  let fileType = typeArray[1];
  if(fileType =='jpg' ||
  fileType =='png' ||
  fileType =='jpeg' ||
  fileType =='gif' ||
  fileType =='pdf'){
    cb(null, true)
  }else{
    cb(null, false)
    req.fileValidationError = "jpg,png,jpeg,gif,pdf 파일만 업로드 가능";
  }
}

let upload2 = multer({
  storage: storage,
  fileFilter : fileFilterImg
});

//필더 만들기
router.post("/uploadtest", upload2.single('test'),function(req, res, next){
  console.log(upload2)
  if(req.fileValidationError){
    res.send(req.fileValidationError)
  }
  
  res.send(req.file);
})


//썸네일 만들기
router.post("/uploadtest2", upload.single('test'),function(req, res, next){
  //console.log(file);
  console.log(req.file);
  console.log(req.body);

  thumb({
    basename:req.file.filename.split(".")[0] + "_sub",
    suffix:'',
    source: req.file.path,
    destination: req.file.destination,
    width: 300
  }).then(function() {
    console.log('Success');
    res.sendStatus(200)
  }).catch(function(e) {
    console.log('Error', e.toString());
    res.sendStatus(500);
  });
  
  //res.send(req.file);
})





module.exports = router;
