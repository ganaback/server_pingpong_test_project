const express = require("express");
const router = express.Router();
const fs = require("fs");
const multer = require("multer");
const path = require("path");

import filesql from "../sql/file_sql";
import msql from "../sql/member_sql";
import util from "../util.js";

let storage = multer.diskStorage({
  destination: function(req, file, callback) {
    callback(null, "profile/");
  },
  filename: function(req, file, callback) {
    let extension = path.extname(file.originalname);
    let basename = path.basename(file.originalname, extension);
    callback(null, basename + "-" + Date.now() + extension);
  }
});

let upload = multer({
  storage: storage
});

// 뷰 페이지 경로
router.get("/show", function(req, res, next) {
  res.render("board");
});

// 프로필 사진 파일 업로드 처리
//form name = profile
router.post("/profileupload", upload.single("profile"), function(
  req,
  res,
  next
) {
  let file = req.file;
  const id = req.body.id;
  const type = 1;
  if(file.mimetype.indexOf('image') == -1){
    res.json({result : false, message :"image file 아님"});
  }else{
    msql
    .findOneUuid(id)
    .then(member => {
      if (!member.mem_profile) {
        filesql
          .upload(id, null, file, type)
          .then(result => {
            msql
              .profileUpload(id, result.fiUuid)
              .then(result => {
                //res.send(result);
                util.resjsionInfo(res, true, "success", "프로필 업로드 완료")
              })
              .catch(err => {
                util.resjsionerr(res, " msql profileUpload", err);
              });
          })
          .catch(err => {
            util.resjsionerr(res, "file upload", err);
          });
      } else {
        filesql
          .findOneFiUuid(member.mem_profile)
          .then(result => {
            fs.unlink(result.fiDestination + result.fiFilename, err => {
              console.log(err);
            });
            filesql
              .update(member.mem_profile, null, file)
              .then(result => {
                util.resjsionInfo(res, true, "success", "프로필 변경 완료")
              })
              .catch(err => {
                util.resjsionerr(res, "file update", err);
              });
          })
          .catch(err => {
            res.json({result : false, message :err});
          });
      }
    })
    .catch(err => {
      res.json({result : false, message :err});
    });
  }
});


// 프로필 배경 사진 파일 업로드 처리
//form name = profile
router.post("/profilebgupload", upload.single("profilebg"), function(
    req,
    res,
    next
  ) {
    let file = req.file;
    const id = req.body.id;
    const type = 2;
    if(file.mimetype.indexOf('image') == -1){
      res.json({result : false, message :"image file 아님"});
    }else{
      msql
      .findOneUuid(id)
      .then(member => {
        if (!member.mem_profileBg) {
          filesql
            .upload(id, null, file, type)
            .then(result => {
              msql
                .profileBgUpload(id, result.fiUuid)
                .then(result => {
                  //res.send(result);
                  util.resjsionInfo(res, true, "success", "프로필 배경 업로드 완료")
                })
                .catch(err => {
                  util.resjsionerr(res, " msql profileUpload", err);
                });
            })
            .catch(err => {
              util.resjsionerr(res, "file upload", err);
            });
        } else {
          filesql
            .findOneFiUuid(member.mem_profileBg)
            .then(result => {
              fs.unlink(result.fiDestination + result.fiFilename, err => {
                console.log(err);
              });
              filesql
                .update(member.mem_profileBg, null, file)
                .then(result => {
                  util.resjsionInfo(res, true, "success", "프로필 배경 변경 완료")
                })
                .catch(err => {
                  util.resjsionerr(res, "file update", err);
                });
            })
            .catch(err => {
              res.json({result : false, message :err});
            });
        }
      })
      .catch(err => {
        res.json({result : false, message :err});
      });
    }
    
  });

module.exports = router;
