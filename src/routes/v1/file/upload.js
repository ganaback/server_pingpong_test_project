const express = require("express");
const router = express.Router();
const fs = require("fs");
const multer = require("multer");
const path = require("path");

import filesql from "../sql/file_sql";
import msql from "../sql/member_sql";
import util from "../util.js";
import fileutil from "../../../utils/fileupload";

const upload = fileutil.profileupload();

// 뷰 페이지 경로
router.get("/show", function(req, res, next) {
  res.render("board");
});

// 프로필 사진 파일 업로드 처리
//form name = profile
router.post("/profileupload", upload.single("profile"), function(
  req,
  res,
  next
) {
  if (req.fileValidationError) {
    res.json({ result: false, message: req.fileValidationError });
  }
  let file = req.file;
  //const id = req.body.id;
  const id = util.getId(req.headers['x-access-token']);
  const type = 1;
  //썸네일 생성
  fileutil
    .thumb(req.file)
    .then(() => {
      console.log("썸네일 생성 성공");
    })
    .catch(err => {
      res.json({ result: false, message: err + "썸네일 생성 실패" });
    })
    .then(
      //기존 사진 확인
      msql
        .findOneUuid(id)
        .then(member => {
          if (!member.mem_profile) {
            //기존파일 없음. 새로 업로드
            filesql
              .upload(id, null, file, type)
              .then(result => {
                msql
                  .profileUpload(id, result.fiUuid)
                  .then(result => {
                    //res.send(result);
                    res.json({ result: true, message: "프로필 업로드 완료" });
                  })
                  .catch(err => {
                    res.json({ result: false, err: err + "" });
                  });
              })
              .catch(err => {
                res.json({ result: false, err: err + "" });
              });
          } else {
            //기존파일 교체
            filesql
              .findOneFiUuid(member.mem_profile)
              .then(result => {
                //기본파일삭제
                fileutil.deletefile(result);
                //썸네일삭제
                fileutil.deletefilethumb(result);
                filesql
                  .update(member.mem_profile, null, file)
                  .then(result => {
                    res.json({ result: true, message: "프로필 변경 완료" });
                  })
                  .catch(err => {
                    res.json({ result: false, err: err + "" });
                  });
              })
              .catch(err => {
                res.json({ result: false, err: err + "" });
              });
          }
        })
        .catch(err => {
          res.json({ result: false, err: err + "" });
        })
    );
});

// 프로필 사진 배경 파일 업로드 처리
//form name = profile
router.post("/profilebgupload", upload.single("profilebg"), function(
  req,
  res,
  next
) {
  if (req.fileValidationError) {
    res.json({ result: false, message: req.fileValidationError });
  }
  let file = req.file;
  //const id = req.body.id;
  const id = util.getId(req.headers['x-access-token']);
  const type = 2;
  //썸네일 생성
  fileutil
    .thumb(req.file)
    .then(() => {
      console.log("썸네일 생성 성공");
    })
    .catch(err => {
      res.json({ result: false, message: err + "썸네일 생성 실패" });
    })

    .then(
      //기존 사진 확인
      msql
        .findOneUuid(id)
        .then(member => {
          if (!member.mem_profileBg) {
            //기존파일 없음. 새로 업로드
            filesql
              .upload(id, null, file, type)
              .then(result => {
                msql
                  .profileBgUpload(id, result.fiUuid)
                  .then(result => {
                    //res.send(result);
                    res.json({
                      result: true,
                      message: "프로필 배경 업로드 완료"
                    });
                  })
                  .catch(err => {
                    res.json({ result: false, err: err + "" });
                  });
              })
              .catch(err => {
                res.json({ result: false, err: err + "" });
              });
          } else {
            //기존파일 교체
            filesql
              .findOneFiUuid(member.mem_profileBg)
              .then(result => {
                //기본파일삭제
                fileutil.deletefile(result);
                //썸네일삭제
                fileutil.deletefilethumb(result);
                filesql
                  .update(member.mem_profileBg, null, file)
                  .then(result => {
                    res.json({
                      result: true,
                      message: "프로필 배경 변경 완료"
                    });
                  })
                  .catch(err => {
                    res.json({ result: false, err: err + "" });
                  });
              })
              .catch(err => {
                res.json({ result: false, err: err + "" });
              });
          }
        })
        .catch(err => {
          res.json({ result: false, err: err + "" });
        })
    );
});

module.exports = router;
