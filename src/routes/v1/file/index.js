module.exports = function(app){//함수로 만들어 객체 app을 전달받음
	var express = require('express');
	var router = express.Router();

	var upload = require('./upload');
	app.use("/file" , upload);

  	var answer_upload = require('./answer_upload');
	app.use("/file" , answer_upload);

	var show = require('./show');
	app.use("/file" , show);
	
	var deleted = require('./deleted');
	app.use("/file" , deleted);

	var uploadtest = require('./uploadtest');
	app.use("/file" , uploadtest);

	app.use("/file" , require('./aws'));
	app.use("/file" , require('./awsImg'));

	return router;	//라우터를 리턴
};