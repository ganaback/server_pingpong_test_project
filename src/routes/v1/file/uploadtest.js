const express = require("express");
const router = express.Router();
const fs = require("fs");
const multer = require("multer");
const path = require("path");
const thumb = require('node-thumbnail').thumb;
import filesql from "../sql/file_sql";
import msql from "../sql/member_sql";
import util from "../util.js";
import { promises, unwatchFile } from "fs";
import fileutil from "../../../utils/fileupload";

router.get("/test111", function(req, res, next){
    
    res.send("test")
})




const upload = fileutil.profileupload();
router.post("/uploadtest3", upload.array('test'),function(req, res, next){
    if(req.fileValidationError){
      res.send(req.fileValidationError)
    }
    res.send(req.files);
  })


const imgupload = fileutil.certification();
router.post("/uploadtest4", imgupload.single('test'),function(req, res, next){
    if(req.fileValidationError){
      res.send(req.fileValidationError)
    }
    fileutil.thumb(req.file)
    .then(function() {
        console.log('Success');
        res.sendStatus(200)
      }).catch(function(e) {
        console.log('Error', e.toString());
        res.sendStatus(500);
      });
   // res.send(req.file);
  })




module.exports = router;
