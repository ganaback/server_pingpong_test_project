const express = require("express");
const router = express.Router();
const fs = require("fs");

var AWS = require("aws-sdk");
AWS.config.region = "ap-northeast-2";
var ec2 = new AWS.EC2();
var s3 = new AWS.S3();
var formidable = require("formidable");
const path = require("path");
const suffix = require("suffix");
import filesql from "../sql/file_sql";

// router.get('/', function(req, res){
//         res.send('Hello world');
// });
router.get("/ec2", function(req, res) {
  ec2.describeInstances({}, function(err, data) {
    res.json(data);
  });
});

//저장
router.get("/put", function(req, res) {
  var param = {
    Bucket: "budyprofile",
    Key: "logo2.png",
    ACL: "public-read",
    Body: fs.createReadStream("./profile/c.jpg"),
    ContentType: "image/png"
  };
  s3.upload(param, function(err, data) {
    console.log(err);
    console.log(data);
  });
});

router.get("/list", function(req, res) {
  s3.listObjects({ Bucket: "budyprofile" })
    .on("success", function handlePage(response) {
      for (var name in response.data.Contents) {
        console.log(response.data.Contents[name].Key);
      }
      if (response.hasNextPage()) {
        response
          .nextPage()
          .on("success", handlePage)
          .send();
      }
      res.json(response.data);
    })
    .send();
});

router.get("/connect", function(req, res) {
  ec2.describeInstances({}, function(err, data) {
    res.json(data);
  });
});

// router.get('/down', function(req, res){
//     var AWS = require('aws-sdk');
//     AWS.config.region = 'ap-northeast-2';
//     var s3 = new AWS.S3();
//     var file = require('fs').createWriteStream('logo.png');
//     var params = {Bucket:'budyprofile', Key:'logo.png'};
//     s3.getObject(params).createReadStream().pipe(file);

// });

router.post("/upload_receiver", function(req, res) {
  var form = new formidable.IncomingForm();
  form.parse(req, function(err, fields, files) {
    var params = {
      Bucket: "budyprofile",
      Key: "profile/" + files.userfile.name,
      ACL: "public-read",
      Body: require("fs").createReadStream(files.userfile.path)
    };
    s3.upload(params, function(err, data) {
      var result = "";
      if (err) result = "Fail";
      else result = `<img src="${data.Location}">`;
      console.log(data);
      console.log(result);
      res.send(`<html><body>${result}</body></html>`);
    });
  });
});

router.post("/upload_receiver2", function(req, res) {
  const id = util.getId(req.headers["x-access-token"]);

  var form = new formidable.IncomingForm();
  form.multiples = true;
  form.parse(req, function(err, fields, files) {
    const fiCer = files.certification;
    console.log(fiCer.size);
    if (fiCer.size != 0) {
      if (!fiCer.length) {
        fiCer.length = 1;
        fiCer[0] = fiCer;
      }
      if (fiCer.length > 3) {
        res.json("3개초과");
      } else {
        let uploadArr = new Array();
        let err;
        for (let i = 0; i < fiCer.length; i++) {
          let typeArray = fiCer[i].type.split("/");
          let fileType = typeArray[1];
          if (
            fileType == "jpg" ||
            fileType == "png" ||
            fileType == "jpeg" ||
            fileType == "gif"
          ) {
            let fileName = suffix(fiCer[i].name, "-" + Date.now());
            fiCer[i].fiFilename = fileName;
            let params = {
              Bucket: "budyprofile",
              Key: "profile/" + fileName,
              ACL: "public-read",
              Body: require("fs").createReadStream(fiCer[i].path)
            };
            uploadArr[i] = s3.upload(params);

            //promiseArr[i] = upload.promise();
            //   console.log("file  갯수 ==>" +fiCer[i].fiFilename);
            //   console.log("file  갯수 ==>" +fiCer[i].path);
            //   console.log("file  갯수 ==>" +fiCer[i].size);
            //   console.log("file  갯수 ==>" +fiCer[i].type);
          } else {
            err = "jpg,png,jpeg,gif 파일만 업로드 가능";
          }
        }
        console.log("file  갯수 ==>" + fiCer.length);
        if (err) {
          res.json(err);
        } else {
          let promiseArr = [];
          let fileArr = [];
          uploadArr.forEach((upload, index) => {
            promiseArr[index] = upload.promise();
            promiseArr[index + uploadArr.length] = filesql.uploadAws(
              id,
              null,
              fiCer[index],
              3,
              "profile/"
            );
          });
          console.log(promiseArr);
          Promise.all(promiseArr)
            .then(data => {
              //console.log(data)
              res.json(data);
            })
            .catch(err => {
              res.send(err + "");
            });
        }
      }
    } else {
      res.send("파일엄슴");
    }
  });
});

router.post("/upload_receiver3", function(req, res) {
  var form = new formidable.IncomingForm();
  form.multiples = true;

  form.parse(req, function(err, fields, files) {
    // console.log(files)
    // console.log(files.userfile)
    console.log(files.userfile.length);
    console.log({
      file: files.userfile[0].name,
      file2: files.userfile[1].name
    });
    var params = {
      Bucket: "budyprofile",
      Key: "profile/" + files.userfile[0].name,
      ACL: "public-read",
      Body: require("fs").createReadStream(files.userfile[0].path)
    };
    var params2 = {
      Bucket: "budyprofile",
      Key: "profile/" + files.userfile[1].name,
      ACL: "public-read",
      Body: require("fs").createReadStream(files.userfile[1].path)
    };
    const up1 = s3.upload(params);
    const up2 = s3.upload(params2);

    const promise1 = up1.promise();
    const promise2 = up2.promise();

    // promise1
    //  .then(data =>{
    //     res.json(data)
    // })
    // .catch(err=>{
    //     res.send(err+"")
    // })

    Promise.all([promise1, promise2])
      .then(data => {
        //console.log(data)
        res.json(data);
      })
      .catch(err => {
        res.send(err + "");
      });
  });
});

router.use(function(err, req, res, next) {
  console.error(err.stack);
  res.status(500).send("Something broke!");
});

router.get("/delete", function(req, res) {
  var params = {
    Bucket: "budyprofile",
    Delete: {
      Objects: [
        {
          Key: "logo.png"
        },
        {
          Key: "logo2.jpg"
        }
      ],
      Quiet: false
    }
  };
  s3.deleteObjects(params, function(err, data) {
    if (err) console.log(err, err.stack);
    // an error occurred
    else console.log(data); // successful response
  });
});

module.exports = router;
