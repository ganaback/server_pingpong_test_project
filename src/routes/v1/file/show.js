const express = require("express");
const router = express.Router();
const fs = require("fs");
import util from "../util.js";


import filesql from "../sql/file_sql";
import msql from "../sql/member_sql";

  router.get("/profileshow", (req, res, next) => {
    const id = req.query.id;
    msql
      .findOneUuid(id)
      .then(member => {
        let fileUuid =  member.mem_profile;
        if (!fileUuid) {
            res.json({ result: false, message: "등록된 파일없음" });
          }else{
            filesql
          .findOneFiUuid(fileUuid)
          .then(result => {
            res.json({result:true, img:result.fiDestination + result.fiFilename})
          })
          .catch(err => {
            res.json({ result: false, message: err +""});
          });
          }
      })
      .catch(err => {
        res.json({ result: false, message: err+""});
      });
  });



router.get("/profilebgshow", (req, res, next) => {
  const id = req.query.id;
    msql
      .findOneUuid(id)
      .then(member => {
        let fileUuid =  member.mem_profileBg;
        if (!fileUuid) {
            res.json({ result: false, message: "등록된 파일없음" });
          }else{
            filesql
          .findOneFiUuid(fileUuid)
          .then(result => {
            res.json({result:true, img:result.fiDestination + result.fiFilename})
          })
          .catch(err => {
            res.json({ result: false, message: err+"" });
          });
          }
        
          
      })
      .catch(err => {
        res.json({ result: false, message: err +""});
      });
  });
  
  module.exports = router;