const express = require("express");
const router = express.Router();

import filesql from "../sql/file_sql";
import msql from "../sql/member_sql";
import util from "../util.js";
import fileutil from "../../../utils/fileupload";

router.post("/profiledelete", (req, res, next) => {
  //const id = req.body.id;
  const id = util.getId(req.headers['x-access-token']);
  msql
    .findOneUuid(id)
    .then(member => {
      const fiUuid = member.mem_profile;
      filesql
        .findOneFiUuid(fiUuid)
        .then(result => {
          if(!result){
            res.json({ result: false, message: "프로필 없음" });
          }
          //기본파일삭제
          fileutil.deletefile(result);
          //썸네일삭제
          fileutil.deletefilethumb(result);
          msql
            .profileUpload(id, null)
            .then(result => {
              filesql
                .deletefile(fiUuid)
                .then(result => {
                  res.json({ result: true, message: "프로필 삭제 완료" });
                })
                .catch(err => {
                  res.json({ result: false, message: err + "" });
                });
            })
            .catch(err => {
              res.json({ result: false, message: err + "" });
            });
        })
        .catch(err => {
          res.json({ result: false, message: err + "" });
        });
    })
    .catch(err => res.json({ result: false, message: err + "" }));
});

router.post("/profilebgdelete", (req, res, next) => {
  const id = util.getId(req.headers['x-access-token']);
  msql
    .findOneUuid(id)
    .then(member => {
      const fiUuid = member.mem_profileBg;
      filesql
        .findOneFiUuid(fiUuid)
        .then(result => {
          if(!result){
            res.json({ result: false, message: "프로필 배경 없음" });
          }
          //기본파일삭제
          fileutil.deletefile(result);
          //썸네일삭제
          fileutil.deletefilethumb(result);
          msql
            .profileBgUpload(id, null)
            .then(result => {
              filesql
                .deletefile(fiUuid)
                .then(result => {
                  res.json({ result: true, message: "프로필 배경 삭제 완료" });
                })
                .catch(err => {
                  res.json({ result: false, message: err + "" });
                });
            })
            .catch(err => {
              res.json({ result: false, message: err + "" });
            });
        })
        .catch(err => {
          res.json({ result: false, message: err + "" });
        });
    })
    .catch(err => res.json({ result: false, message: err + "" }));
});

module.exports = router;
