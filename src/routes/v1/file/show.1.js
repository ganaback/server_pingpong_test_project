const express = require("express");
const router = express.Router();
const fs = require("fs");
import util from "../util.js";


import filesql from "../sql/file_sql";
import msql from "../sql/member_sql";

//백업용
// router.get("/profileshow", (req, res, next) => {
//     const id = req.body.id;
//     msql
//       .findOneUuid(id)
//       .then(member => {
//         let fileUuid =  member.mem_profile;
//         if (!fileUuid) {
//             res.json({ result: false, message: "등록된 파일없음" });
//           }
//         filesql
//           .findOneFiUuid(fileUuid)
//           .then(result => {
//             res.json({result:true, img:result.fiDestination + result.fiFilename})
//               // fs.readFile(result.fiDestination + result.fiFilename, function(
//               //   error,
//               //   data
//               // ) {
//               //   res.writeHead(200, { "Context-Type": "text/html" });
//               //   res.end(data);
//               // });
//           })
//           .catch(err => {
//             res.send(err + "");
//           });
//       })
//       .catch(err => {
//         res.send(err + "");
//       });
//   });

  router.get("/profileshow", (req, res, next) => {
    const id = req.query.id;
    msql
      .findOneUuid(id)
      .then(member => {
        let fileUuid =  member.mem_profile;
        if (!fileUuid) {
            res.json({ result: false, message: "등록된 파일없음" });
          }
        filesql
          .findOneFiUuid(fileUuid)
          .then(result => {
            res.json({result:true, img:result.fiDestination + result.fiFilename})
              // fs.readFile(result.fiDestination + result.fiFilename, function(
              //   error,
              //   data
              // ) {
              //   res.writeHead(200, { "Context-Type": "text/html" });
              //   res.end(data);
              // });
          })
          .catch(err => {
            res.send(err + "");
          });
      })
      .catch(err => {
        res.send(err + "");
      });
  });



router.get("/profilebgshow", (req, res, next) => {
  const id = req.query.id;
    msql
      .findOneUuid(id)
      .then(member => {
        let fileUuid =  member.mem_profileBg;
        if (!fileUuid) {
            res.json({ result: false, message: "등록된 파일없음" });
          }
        filesql
          .findOneFiUuid(fileUuid)
          .then(result => {
            res.json({result:true, img:result.fiDestination + result.fiFilename})
              // fs.readFile(result.fiDestination + result.fiFilename, function(
              //   error,
              //   data
              // ) {
              //   res.writeHead(200, { "Context-Type": "text/html" });
              //   res.end(data);
              // });
          })
          .catch(err => {
            res.send(err + "");
          });
      })
      .catch(err => {
        res.send(err + "");
      });
  });
  
  module.exports = router;