const express = require("express");
const router = express.Router();
const fs = require("fs");
const multer = require("multer");
const path = require("path");

import filesql from "../sql/file_sql";
import msql from "../sql/member_sql";
import util from "../util.js";
import fileutil from "../../../utils/fileupload";
var thumb = require('node-thumbnail').thumb;

var AWS = require("aws-sdk");
AWS.config.region = "ap-northeast-2";
var ec2 = new AWS.EC2();
var s3 = new AWS.S3();
var formidable = require("formidable");
const suffix = require("suffix");

const upload = fileutil.profileupload();

var gm = require('gm');

const paramsAws = (file, destiantion, fileName) => {
  let awsParams = {
    Bucket: "budyprofile",
    Key: destiantion + fileName,
    ACL: "public-read",
    Body: require("fs").createReadStream(file.path),
    ContentType: file.type
  };
  return awsParams;
};

router.get("/ec2test", function(req, res) {
  ec2.describeInstances({}, function(err, data) {
    res.json(data);
  });
});

router.post("/profileupload2", function(req, res) {
  var form = new formidable.IncomingForm();
  form.parse(req, function(err, fields, files) {
    //console.log(files);
    let file = files.profile;
    //console.log(file);
    if (!file.name || file.size === 0) {
      return res.send("file 없음");
    } 
    let typeArray = file.type.split("/");
    let fileType = typeArray[1];
    if (
      fileType == "jpg" ||
      fileType == "png" ||
      fileType == "jpeg" ||
      fileType == "gif"
    ){
        let fileName = suffix(file.name, "-" + Date.now());
        file.fiFilename = fileName;
        const destination = "profile/";
        const params = paramsAws(file, destination, fileName);

        // const th = gm(fileName)
        //     .thumb(100, 100, 'imgs/thumb.jpg', function (err) {
        //     if (err) console.error(err);
        //     else console.log('done - thumb');
        // });
        // res.send(th)

        //   thumb({
        //     suffix:'_thumb',
        //     source: file.path,
        //     destination: destination,
        //     width: 80
        //   }, function(files, err, stdout, stderr) {
        //     console.log('All done!');
        //       res.send(files)          
        //   })
        //   .then(result=>{
        //     res.send(result)
        //   })
        //   .catch(err=>{
        //       res.send(err)
        //   })
    
        s3.upload(params, function(err, data) {
          var result = "";
          if (err) result = "Fail";
          // else result = data;
          // res.send(data);
          else result = `<img src="${data.Location}">`;
          console.log(data.Location);
          res.send(`<html><body>${result}</body></html>`);
        });
    }else{
        return res.send("이미지 파일 아님")
    }

    
  });
});

// 프로필 사진 파일 업로드 처리
//form name = profile
router.post("/profileupload", upload.single("profile"), function(
  req,
  res,
  next
) {
  if (req.fileValidationError) {
    res.json({ result: false, message: req.fileValidationError });
  }
  let file = req.file;
  //const id = req.body.id;
  const id = util.getId(req.headers["x-access-token"]);
  const type = 1;
  //썸네일 생성
  fileutil
    .thumb(req.file)
    .then(() => {
      console.log("썸네일 생성 성공");
    })
    .catch(err => {
      res.json({ result: false, message: err + "썸네일 생성 실패" });
    })
    .then(
      //기존 사진 확인
      msql
        .findOneUuid(id)
        .then(member => {
          if (!member.mem_profile) {
            //기존파일 없음. 새로 업로드
            filesql
              .upload(id, null, file, type)
              .then(result => {
                msql
                  .profileUpload(id, result.fiUuid)
                  .then(result => {
                    //res.send(result);
                    res.json({ result: true, message: "프로필 업로드 완료" });
                  })
                  .catch(err => {
                    res.json({ result: false, err: err + "" });
                  });
              })
              .catch(err => {
                res.json({ result: false, err: err + "" });
              });
          } else {
            //기존파일 교체
            filesql
              .findOneFiUuid(member.mem_profile)
              .then(result => {
                //기본파일삭제
                fileutil.deletefile(result);
                //썸네일삭제
                fileutil.deletefilethumb(result);
                filesql
                  .update(member.mem_profile, null, file)
                  .then(result => {
                    res.json({ result: true, message: "프로필 변경 완료" });
                  })
                  .catch(err => {
                    res.json({ result: false, err: err + "" });
                  });
              })
              .catch(err => {
                res.json({ result: false, err: err + "" });
              });
          }
        })
        .catch(err => {
          res.json({ result: false, err: err + "" });
        })
    );
});

module.exports = router;
