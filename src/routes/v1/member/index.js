module.exports = function(app){//함수로 만들어 객체 app을 전달받음
	var express = require('express');
	var router = express.Router();



  var join = require('./join');
	app.use("/users" , join);

	var login = require('./login');
	app.use("/users" , login);

	var updateInfo = require('./updateInfo');
	app.use("/users" , updateInfo);

	var updatePw = require('./updatePw');
	app.use("/users" , updatePw);

	var checkUniqe = require('./checkUniqe');
	app.use("/users" , checkUniqe);

	var secession = require('./secession');
	app.use("/users" , secession);

	var mypage = require('./mypage');
	app.use("/users" , mypage);


	app.use("/users" , require('./education'));
	app.use("/users" , require('./employment'));
	app.use("/users" , require('./location'));
	app.use("/users" , require('./login2'));

	var test = require('./test');
	app.use("/users" , test);
	
	
	return router;	//라우터를 리턴
};