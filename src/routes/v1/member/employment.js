const express = require("express");
const router = express.Router();

const util = require("../util");
import filesql from "../sql/file_sql";
import budsql from "../sql/budtender_sql";
import { promises } from "fs";
import models, { Sequelize } from '../../../models'


//생성
router.post("/employment", function(req, res, next){
    const id = util.getId(req.headers["x-access-token"]);
    const body = req.body;
    models.employment.create({
        mem_uuid :id,
        position : body.position,
        company : body.company,
        startYear : body.startYear,
        endYear : body.endYear,
        isWork : body.isWork
    })
    .then(result=>{
        res.json({result:true, message:result})
    })
    .catch(err=>{
        res.json({result:false, message:err});
    })
})


//수정
router.put("/employment", function(req, res, next){
    const id = util.getId(req.headers["x-access-token"]);
    const body = req.body;
    models.employment.update({
        position : body.position,
        company : body.company,
        startYear : body.startYear,
        endYear : body.endYear,
        isWork : body.isWork
    },{
        where:{mem_uuid :id, empUuid:body.empUuid}    
    })
    .then(result=>{
        res.json({result:true, message:result})
    })
    .catch(err=>{
        res.json({result:false, message:err});
    })
})


//삭제
router.delete("/employment", function(req, res, next){
    const id = util.getId(req.headers["x-access-token"]);
    const body = req.body;
    models.employment.destroy({
        where:{mem_uuid :id, empUuid:body.empUuid}    
    })
    .then(result=>{
        res.json({result:true, message:result})
    })
    .catch(err=>{
        res.json({result:false, message:err});
    })
})

//조회
router.get("/employment", function(req, res, next){
    const id = util.getId(req.headers["x-access-token"]);
    
    models.employment.findAll({
        where:{mem_uuid :id}        
    })
    .then(result=>{
        res.json({result:true, message:result})
    })
    .catch(err=>{
        res.json({result:false, message:err});
    })
})







module.exports = router;
