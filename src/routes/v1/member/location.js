const express = require("express");
const router = express.Router();

const util = require("../util");
import filesql from "../sql/file_sql";
import budsql from "../sql/budtender_sql";
import { promises } from "fs";
import models, { Sequelize } from '../../../models'


//생성
router.post("/location", function(req, res, next){
    const id = util.getId(req.headers["x-access-token"]);
    const body = req.body;
    models.location.create({
        mem_uuid :id,
        locationName : body.locationName,
        startYear : body.startYear,
        endYear : body.endYear,
        isLive : body.isLive
    })
    .then(result=>{
        res.json({result:true, message:result})
    })
    .catch(err=>{
        res.json({result:false, message:err});
    })
})


//수정
router.put("/location", function(req, res, next){
    const id = util.getId(req.headers["x-access-token"]);
    const body = req.body;
    models.location.update({
        locationName : body.locationName,
        startYear : body.startYear,
        endYear : body.endYear,
        isLive : body.isLive
    },{
        where:{mem_uuid :id, locaUuid:body.locaUuid}    
    })
    .then(result=>{
        res.json({result:true, message:result})
    })
    .catch(err=>{
        res.json({result:false, message:err});
    })
})


//삭제
router.delete("/location", function(req, res, next){
    const id = util.getId(req.headers["x-access-token"]);
    const body = req.body;
    models.location.destroy({
        where:{mem_uuid :id, locaUuid:body.locaUuid}    
    })
    .then(result=>{
        res.json({result:true, message:result})
    })
    .catch(err=>{
        res.json({result:false, message:err});
    })
})

//조회
router.get("/location", function(req, res, next){
    const id = util.getId(req.headers["x-access-token"]);
    
    models.location.findAll({
        where:{mem_uuid :id}        
    })
    .then(result=>{
        res.json({result:true, message:result})
    })
    .catch(err=>{
        res.json({result:false, message:err});
    })
})



module.exports = router;
