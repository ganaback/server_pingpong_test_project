const express = require("express");
const router = express.Router();

import util from "../util.js";
import msql from "../sql/member_sql";

//email조회
router.get("/email/:email", function(req, res, next){
  let email = req.params.email;
  msql
    .findOneEmail(email)
    .then(function(data) {
      //console.log(data);
      if (data!=null && data !=undefined) {
        // res.json({result:data});
        if (!data.mem_type) {
          res.json({ result: true, message: "미인증 회원, 인증메일 재전송 가능" });
        } else {
          //res.send("인증됨");
          res.json({result: false, message: "가입된 아이디."});
        }
      } else {
        res.json({ result: true, message: "사용 가능한 아이디 입니다." });
      }
    });
});


//nickname조회
router.get("/nick/:nick", function(req, res, next) {
  let nick = req.params.nick;
  msql.findOneNickname(nick)
    .then(function(data) {
      if ((data == null || data == undefined) == false) {
        res.json({ result: false, message: "이미 사용중인 닉네임 입니다." });
      } else {
        res.json({ result: true, message: "사용 가능한 닉네임 입니다." });
      }
    });
});



module.exports = router;
