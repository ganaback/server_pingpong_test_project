const express = require("express");
const router = express.Router();
const models = require("../../../models");
import msql from "../sql/member_sql";
import filesql from "../sql/file_sql";
import catesql from "../sql/category_sql";
import util from "../util.js";
import { promises } from "fs";

router.get("/mypage", function(req, res, next){
    const uuid = req.query.id;
    msql.findOneMyinfo(uuid)
    .then(result =>{
        let link = result.mem_links;
        if(link){
          link = link.split(',');
          result.mem_links =  link;
        }
        const fileArr = new Array();
        const fileSqlArr = new Array();
        fileArr[0] = result.mem_profile;
        fileArr[1] = result.mem_profileBg;
        for(let i=0; i<2;i++){
          if(fileArr[i]){
            fileSqlArr[i] = filesql.findOneFiUuid(fileArr[i]);
          }          
        }
        Promise.all([...fileSqlArr])
          .then(prfofile=>{
            result.mem_profile =prfofile[0];
            result.mem_profileBg =prfofile[1];
            return result;
          }) 
          .then(result=>{
            const inter = result.mem_interested;
            if(inter){
              let interArr =  inter.split(',');
              const selectcate = interArr.map((val)=>{
                return catesql.selectId(val);
              })
              Promise.all([...selectcate])
              .then(category=>{
                result.mem_interested =category;
                res.json(result);
              }) 
            }else{
              res.json(result);
            } 

          })
        
    })
    .catch(err =>{
        res.send(err+"");
    })
})



router.get("/mypage/all", (req, res, next) => {
    const id = util.getId(req.headers["x-access-token"]);
    msql
      .getAllInfo(id)
      .then(result => {
        const m = result[0];
        const b = result[1];
        const l = result[2];
        const ed = result[3];
        const em = result[4];

        const inter = m.mem_interested;
        if(inter){
          let interArr =  inter.split(',');
          const selectcate = interArr.map((val)=>{
            return catesql.selectId(val);
          })
          Promise.all([...selectcate])
          .then(category=>{
            m.mem_interested =category;
            res.json({
              result: true,
              member: m,
              budtender: b,
              location: l,
              educaton: ed,
              employment: em
            });
          }) 
        }else{
          res.json({
            result: true,
            member: m,
            budtender: b,
            location: l,
            educaton: ed,
            employment: em
          });
        }
      })
      .catch(err => {
        res.json({ result: fale, err: err + "" });
      });
  });
  

module.exports = router;
