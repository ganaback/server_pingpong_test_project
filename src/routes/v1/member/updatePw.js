const express = require("express");
const router = express.Router();

import util from "../util.js";
import msql from "../sql/member_sql";

router.post("/newpw", function(req, res, next) {
  let email = req.body.email;
  msql
    .findOneEmail(email)
    .then(function(data) {
      if (!data) {
        res.json({ result: false, message: "존재하지 않는 이메일" });
      }
      if(data.mem_type == 0){
        res.json({ result: false, message: " 미인증 이메일, 재가입처리" });
      }
      // if(!data.mem_droptime){
      //   res.json({ result: false, message: "휴먼회원, 휴먼상태해제필요" });
      // }
      let token = Math.round(
        new Date().valueOf() * Math.random() * Math.random()
      );
      msql
        .updateToken(email, token)
        .then(result => {
          util.emailCheckPw(email, token, res);
          //res.json({ result: true, message: token });
        })
        .catch(err => {
          res.json({ result: false, message: err + "" });
        });
    })
    .catch(err => {
      res.json({ result: false, message: err + "" });
    });
});

router.get("/chpw", function(req, res, next) {
  let email = req.query.email;
  let token = req.query.token;
  msql
    .findOneEmailAndToken(email, token)
    .then(result => {
      if (!result) {
        res.json({ result: false, message: "인증 코드 에러" });
      } else {
        msql
          .updateTemporaryPw(email, token)
          .then(result => {
            res.json({ result: true, message: "임시 비밀번호로 변경" });
          })
          .catch(err => {
            res.json({ result: false, message: err + "" });
          });
      }
    })
    .catch(err => {
      res.json({ result: false, message: err + "" });
    });
});

module.exports = router;
