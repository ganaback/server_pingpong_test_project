const express = require("express");
const router = express.Router();
import util from "../util.js";
import msql from "../sql/member_sql";

  
 
var passport = require('passport')
  , LocalStrategy = require('passport-local').Strategy;

  router.get("/login4", function(req, res, next) {
    res.render("board");
  });

  router.get("/login5", function(req, res, next) {
    
    console.log(req.isAuthenticated());
   
    //로그인 확인 쿼리
    if(req.isAuthenticated() && req.headers['x-access-token']){
      const id = util.getId(req.headers['x-access-token']);
      const user1 = util.getId(req.user);
      if(req.user === req.headers['x-access-token']){
        res.send(req.user + "//" +user1);
      }else{
        res.send("비정상적 로그인" + user1);
      }
    }else{
      res.send("로그인안됨");
    }
    
  });
  

router.post('/login3', function(req, res, next) {
  passport.authenticate('local', function(err, user, info) {
    if(err){
      return res.send(err);
    }
    if(!user){
      console.log(req.isAuthenticated());
      return res.json(info)
    }
    return req.login(user, (loginError) => {
      if (loginError) {
        console.error(loginError);
        return next(loginError);
      }
      console.log(req.isAuthenticated());
      console.log(user);
      console.log(info.member.mem_email)
      //res.send(user);
      util.createLog(56,info.member.mem_uuid,null,req);
      //util.loginToken(user, res);
      return res.json({ result: true, message: user , info : "로그인"});
    });
  })(req, res, next);
});

router.get('/facebook/login', function(req, res){
  res.render('login');
})


router.get('/facebook', passport.authenticate('facebook', {
  scope:'email'
}));

// router.get('/facebook/callback', passport.authenticate('facebook', { successRedirect :'/users/login',
//                                                                          failureRedirect:'/users/login'}));



// router.get('/facebook/callback', passport.authenticate('facebook', function(err, user, info) {
//        console.log("facebook222");
//        res.send(user)
// }));


router.get('/facebook/callback', function(req, res, next) {
  console.log("facebook1");
 
  passport.authenticate('facebook', function(err, email, facebookId) {
    console.log("facebook222");
    res.send(email+ "///"+ facebookId)
    // if(err){
    //   return res.send(err);
    // }
    // if(!user){
    //   console.log(req.isAuthenticated());
    //   return res.json(info);
    // }
    // return req.login(user, (loginError) => {
    //   if (loginError) {
    //     console.error(loginError);
    //     return next(loginError);
    //   }
    //   return res.send(user)
      // console.log(req.isAuthenticated());
      // console.log(user);
      // console.log(info.member.mem_email)
      // //res.send(user);
      // util.createLog(56,info.member.mem_uuid,null,req);
      // //util.loginToken(user, res);
      // return res.json({ result: true, message: user , info : "로그인"});
  //   });
  })(req, res, next);
});



//로그아웃
router.get("/logout2", function(req, res, next) {
  // const id = util.getId(req.headers['x-access-token']);
  // util.createLog(57,id,null,req);
  req.logout();
  req.session.destroy();
  console.log(req.user);
  console.log(req.isAuthenticated());
  res.json({ isAuthenticated:req.isAuthenticated()+"",result:req.user})
  //res.json({ result: true });
});

module.exports = router;
