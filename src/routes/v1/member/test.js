
const express = require("express");
const router = express.Router();

import util from "../util.js";
const models = require("../../../models");



router.post("/type", function(req, res, next){
    const email = req.body.email;
    models.member.update(
        {
          mem_type : req.body.type 
        },
        {
          where: { mem_email: email }
        }
      )
      .then(result =>{
          res.send(result)
      })
      .catch(err =>{
        res.send(err)
      })
})

module.exports = router;
