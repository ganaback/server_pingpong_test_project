const express = require("express");
const router = express.Router();

import util from "../util.js";
import msql from "../sql/member_sql";
import { promises } from "fs";

//휴먼회원처리
router.post("/secession", function(req, res, next) {
  const uuid = req.body.id;
  msql
    .secession(uuid)
    .then(result => {
      util.createLog(58, uuid, null, req);
      util.resjsion(res, result);
    })
    .catch(err => res.send(err + ""));
});

//휴먼회원 복귀처리
router.post("/comeback", function(req, res, next) {
  const uuid = req.body.id;
  msql
    .comeback(uuid)
    .then(result => {
      util.createLog(59, uuid, null, req);
      util.resjsionInfo(res, true, "success", "휴먼회원 해제");
    })
    .catch(err => res.send(err + ""));
});

router.post("/dropmember", function(req, res, next) {
  const uuid = req.body.id;

  let backup = msql
    .findOneUuid(uuid)
    .then(member => {
      msql
        .deleteMemberBackup(member)
        .then(result => {
          msql
            .dropMember(uuid)
            .then(res.json({ result: true, message: "탙퇴됨", info: result }))
            .catch(err => {
              res.json({ result: false, err: err });
            });
        })
        .catch(err => {
          res.json({ result: false, err: err });
        });
    })
    .catch(err => {
      res.json({ result: false, err: err });
    });
});

module.exports = router;
