const express = require("express");
const router = express.Router();

import util from "../util.js";
import msql from "../sql/member_sql";

router.post("/detail", function(req, res, next) {
  let body = req.body;
  msql
    .findOneUuid(body.id)
    .then(result => {
      if (result.mem_type == 0) {
        res.json({ result: false, message: "미인증 이메일" });
      } else {
        msql
          .updateDeail(body)
          .then( () => {
            util.createLog(54,body.id,null,req);
            util.loginToken2(result,body.nickname, res);
            //res.json({ result: true, message: "success" });
          })
          .catch(err => {
            res.json({ result: false, message: err + "" });
          });
      }
    })
    .catch(err => res.json({ result: false, message: err + "" }));
});

router.post("/updateinfo", function(req, res, next) {
  let body = req.body;
  msql
    .findOneUuid(body.id)
    .then(result => {
      if (result.mem_type == 0) {
        res.json({ result: false, message: "미인증 이메일" });
      } else {
        msql
          .updateUserInfo(body)
          .then(result => {
            util.createLog(55,body.id,null,req);
            res.json({ result: true, message: "success" });
          })
          .catch(err => {
            res.json({ result: false, message: err + "" });
          });
      }
    })
    .catch(err => res.json({ result: false, message: err + "" }));
});

module.exports = router;
