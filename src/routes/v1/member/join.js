const express = require("express");
const router = express.Router();

import util from "../util.js";
import msql from "../sql/member_sql";

router.post("/sign_up", (req, res, next) => {
  const body = req.body;
  //정규식 일단 킵
  // const regexemail = /([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
  // const regexpw = /(?=.*\d{1,50})(?=.*[~`!@#$%\^&*()-+=]{1,50})(?=.*[a-zA-Z]{2,50}).{8,50}$/;
  // if(!regexemail.test(body.email)){
  //   res.json({result:false , message:3, info:"email 정규식"})
  // }
  // if(!regexpw.test(body.pw)){
  //   res.json({result:false , message:3, info:"pw 정규식"})
  // }

  msql
    .findOneEmail(body.email)
    .then(result => {
      if (result) {
        //회원가입된 아이디
        if (result.mem_type == 0) {
          //회원가입 인증 안된 아이디, 이메일 재전송
          util.createLog(52, result.mem_uuid, null, req);
          msql.updateTokenAndEmail(body, res);
        } else if (result.mem_type == 1) {
          //이메일 회원 가입된 회원
          res.json({
            result: false,
            message: 1,
            info: "Existing email members"
          });
        } else if (result.mem_type == 2) {
          //페이스북회원 가입된 회원
          res.json({
            result: false,
            message: 2,
            info: "Existing facebook member"
          });
        }
      }
      //회원가입
      else {
        msql
          .joinEmailMember(body)
          .then(result => {
            util.createLog(51, result.mem_uuid, null, req);
            util.joinEmailMember(body.email, result.mem_token, res, "");
          })
      }
    })
    .catch(err => {
      res.json({ result: false, message: err + "" });
    });
});

router.get("/auth", function(req, res, next) {
  let email = req.query.email;
  let token = req.query.token;
  msql
    .findOneEmail(email)
    .then(member => {
      if (!member) {
        //인증에러
        return "http://192.168.0.118:8080/fail";
      } else {
        if (member.mem_token == token) {
          if (member.mem_type == 0) {
            //인증성공
            msql.updatType(member.mem_uuid, 1);
            util.createLog(53, member.mem_uuid, null, req);
            return "http://192.168.0.118:8080/auth";
          } else {
            //이미 인증
            return "http://192.168.0.118:8080/overlap";
          }
        } else {
          //인증에러
          return "http://192.168.0.118:8080/fail";
        }
      }
    })
    .then(result => {
      res.statusCode = 302;
      res.setHeader("Location", result);
      res.end();
    })
    .catch(err => {
      res.json({ result: false, message: err + "" });
    });
});

module.exports = router;
