const express = require("express");
const router = express.Router();
import models, { Sequelize } from '../../../models'

const util = require("../util");

router.post("/education", function(req, res, next){
    const id = util.getId(req.headers["x-access-token"]);
    const body = req.body;
    models.education.create({
        mem_uuid :id,
        school : body.school,
        major : body.major,
        degree : body.degree,
        graduationYear : body.endDate
    })
    .then(result=>{
        res.json({result:true, message:result})
    })
    .catch(err=>{
        res.json({result:false, message:err});
    })
})


router.put("/education", function(req, res, next){
    const id = util.getId(req.headers["x-access-token"]);
    const body = req.body;
    models.education.update({
       
        school : body.school,
        major : body.major,
        degree : body.degree,
        graduationYear : body.endDate
    },{
        where:{mem_uuid :id, eduUuid:body.eduUuid}    
    })
    .then(result=>{
        res.json({result:true, message:result})
    })
    .catch(err=>{
        res.json({result:false, message:err});
    })
})

router.delete("/education", function(req, res, next){
    const id = util.getId(req.headers["x-access-token"]);
    const body = req.body;
    models.education.destroy({
        where:{mem_uuid :id, eduUuid:body.eduUuid}    
    })
    .then(result=>{
        res.json({result:true, message:result})
    })
    .catch(err=>{
        res.json({result:false, message:err});
    })
})


router.get("/education", function(req, res, next){
    const id = util.getId(req.headers["x-access-token"]);
    
    models.education.findAll({
        where:{mem_uuid :id}        
    })
    .then(result=>{
        res.json({result:true, message:result})
    })
    .catch(err=>{
        res.json({result:false, message:err});
    })
})



module.exports = router;
