const express = require("express");
const router = express.Router();
import util from "../util.js";
import msql from "../sql/member_sql";

router.post("/login", function(req, res, next) {
  let body = req.body;
  //정규식 일단 킵
  // const regexemail = /([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
  // const regexpw = /(?=.*\d{1,50})(?=.*[~`!@#$%\^&*()-+=]{1,50})(?=.*[a-zA-Z]{2,50}).{8,50}$/;
  // if(!regexemail.test(body.email)){
  //   res.json({result:false , message:3, info:"email 정규식"})
  // }
  // if(!regexpw.test(body.pw)){
  //   res.json({result:false , message:3, info:"pw 정규식"})
  // }
  msql
    .findOneEmail(body.email)
    .then(result => {
      if (result) {
        let dbPassword = result.mem_pw;
        const hashPassword = util.makeHashPw(body.pw, result.salt);

        if (dbPassword === hashPassword) {
          //로그인 처리
          if(result.mem_type ==0){
            res.json({ result: false, message: 1 ,info :"미인증 회원"});
          }else{
            if(!result.mem_droptime){
              util.createLog(56,result.mem_uuid,null,req);
              util.loginToken(result, res);
            }else{
              res.json({ result: false, message:  2 ,info :"휴먼 회원" });
            }            
          }
        } else {
          res.json({ result: false, message:  3 ,info :"비밀번호 불일치" });
        }
      } else {
        res.json({ result: false, message:  4 ,info :"없는 이메일" });
      }
    })
    .catch(err => {
      res.json({ result: false, err: err + "" });
    });
});

//로그아웃
router.get("/logout", function(req, res, next) {
  // req.session.destroy();
  // res.clearCookie("user");
  const id = util.getId(req.headers['x-access-token']);
  util.createLog(57,id,null,req);
  res.json({ result: true });
  //res.redirect("/users/login");
});

module.exports = router;
