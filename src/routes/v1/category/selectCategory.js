const express = require("express");
const router = express.Router();

import util from "../util.js";
import category_sql from "../sql/category_sql";
const sequelize = require("sequelize");
const Op = sequelize.Op;


const models = require("../../../models");

router.get("/searchcategory", function(req, res, next){
    //let ca = req.body.ca;
    console.log(ca)
    models.category.findAll(
        {
            where: { categoryName: {[Op.like] : '%'+ca+'%'}}
        }
    )
    .then(result=>{
       // console.log(result);
       
        res.send(result);
    })
})


router.get("/selectcategory", function(req, res, next){
    let body = req.body;

    category_sql.selectCategory()
    .then(result =>{
        res.send(result);
        
    })
})

router.get("/selectcategorywithcount", function(req, res, next){
    let body = req.body;

    category_sql.selectCategoryWithCount()
    .then(result =>{
        res.send(result);
    })
})





module.exports = router;
