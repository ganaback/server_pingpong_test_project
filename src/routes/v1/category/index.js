module.exports = function(app){//함수로 만들어 객체 app을 전달받음
	var express = require('express');
	var router = express.Router();



  	var insertCategory = require('./insertCategory');
	app.use("/category" , insertCategory);

	var selectCategory = require('./selectCategory');
	app.use("/category" , selectCategory);

	

	
	return router;	//라우터를 리턴
};