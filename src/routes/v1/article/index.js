/*********************
 * @author 김정래
 * 작성일 : 2019-07-03
 * Article 전용 router
 *********************/

import controller from './article.controller'

module.exports = function(app){//함수로 만들어 객체 app을 전달받음
	var express = require('express');
    var router = express.Router();
    router.post('/create',controller.createArticle) ;
    router.get('/select',controller.selectArticle) ;
    router.post('/modify',controller.modifyArticle) ;
    
	return router;	//라우터를 리턴
};