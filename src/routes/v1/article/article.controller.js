/*********************
 * @author 김정래
 * 작성일 : 2019-06-12
 * POST Article 전용 controller
 *********************/
import models, { Sequelize } from '../../../models'
import makeuuid from '../../../utils/makeuuid'
import sequelize from 'sequelize';
import jwtverify from '../../../utils/jwtverify'
const Op = Sequelize.Op;
const util = require("../util");

/******************
 * Article 생성
 * 
 ******************/
exports.createArticle = (req, res, next) => {
    const body = req.body;
    const token = req.headers['x-access-token'];
    
     try{

         const payload = jwtverify.verify(token);
         if(body.title==undefined || body.body==undefined) throw new Error("필수 파라미터가 유효하지 않습니다."); 

         models.article.create({
            title:body.title,
            body:body.body,
            mem_uuid:payload.id
        })
        .then(result=>{
            util.createLog(70,payload.id,result.articleUuid,req);
            res.json({result:true,message:"create success"});
        })  
        .catch(err=>{
            res.json({result:false,message:err.message});
        });  
 
         //로그남기기
     }
     catch(err){
         res.json({result:false,message:err.message});
     }
 }

 exports.selectArticle = (req, res, next) => {
    const token = req.headers['x-access-token'];
     try{
         const payload = jwtverify.verify(token);
        
         models.article.findOne({
            where:{mem_uuid:payload.id}
        })
        .then(result=>{
            if(result){
                res.json({result:true,message:result});
            }else{
                res.json({result:false,message:"article 없음"});
            }
            
        })  
        .catch(err=>{
            res.json({result:false,message:err.message});
        });  
 
         //로그남기기
     }
     catch(err){
         res.json({result:false,message:err.message});
     }
 }


 exports.modifyArticle = (req, res, next) => {
    const body = req.body;
    const token = req.headers['x-access-token'];
    
         const payload = jwtverify.verify(token);
         models.article.findOne({
            where:{mem_uuid:payload.id}
        })
        .then(result=>{
           if(!result){
                res.json({result:false,message:"article 없음"});
                return null;
           }else{
              return Promise.resolve(models.article.update({
                    title:body.title,
                    body:body.body
                },
                {where: {mem_uuid:payload.id, articleUuid :body.articleUuid}})
                .then(result=>{
                    util.createLog(71,payload.id,body.articleUuid,req);
                    res.json({result:true,message:result});
                    return null;
                }) )
                .catch(err=>{
                    res.json({result:false,message:err.message});
                    return null;
                });         
           }
        })
        .catch(err=>{
            res.json({result:false,message:err.message});
        });  
 
     
 }
 