/*********************
 * @author 김정래
 * 작성일 : 2019-06-28
 * support 전용 router
 *********************/
import fileupload from '../../../utils/fileupload'
import multer from 'multer'
import controller from './support.controller'
import path  from 'path'



let storage = multer.diskStorage({
    destination: function(req, file, callback) {
      callback(null, "supportAttach/");
    },
    filename: function(req, file, callback) {
      let extension = path.extname(file.originalname);
      let basename = path.basename(file.originalname, extension);
      callback(null, basename + "-" + Date.now( ) + extension);
    }
});

let upload = multer({
    storage:storage
});

/** Router 
 * 
 * 영역
 * 
 * * */

module.exports = function(app){
	var express = require('express');
    var router = express.Router();
    router.post('/add',upload.array("supportAttach",5),controller.insertSupport) ;
    router.get('/test',function(req,res,next){
        res.render('supporttest')
    }) ;
	return router;	
};