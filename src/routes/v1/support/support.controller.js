/*********************
 * @author 김정래
 * 작성일 : 2019-06-28
 * support 전용 controller
 *********************/
import models, { Sequelize } from '../../../models'
import sequelize from 'sequelize';
import jwtverify from '../../../utils/jwtverify'

const con = models.sequelize;
const Op = Sequelize.Op;


/**
 * support 전송
 * 파일은 5개까지 가능.
 * 
 *  */

exports.insertSupport = (req, res, next) => {
    const body = req.body;
    let param = {
        title:body.title,
        email:body.email,
        name:body.name,
        description:body.description
    }
    let sUuid;

    try {
        if(req.files.length>5) throw new Error("File Count error");

        models.support.create(param)
        .then(result=>{
            sUuid = result.sUuid;
            let promises  = [];
            let files = req.files;
           
            for(let i=0; i<files.length; i++){
                let newPromise = models.file.create({
                    fiOriginname: files[i].originalname,
                    fiFilename: files[i].filename,
                    fiSize: files[i].size,
                    fiType: files[i].mimetype,
                    fiDestination: files[i].destination,
                    supportId:sUuid
                })
                promises.push(newPromise);
            }

            return Promise.all(promises)
        })  
        .then(result=>{
            res.json({result:true,message:"Completed"});
        })
        .catch(err=>{
            res.json({result:false,message:err.message});
        }); 

       

    } catch (err) {
        res.json({ result: false, message: "Server error:"+ err });
    }
}
