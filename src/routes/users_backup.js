//const express = require("express");
import express from 'express';
const models = require("../models");
const crypto = require("crypto");
const nodemailer = require("nodemailer");
const uuid4 = require("uuid4");

const router = express.Router();

let jwt = require("jsonwebtoken");
let secretObj = require("../config/jwt");

// router.get("/test",function(){
  
//   console.log(makeUuid4());
// })
// const uuid4 = () => {
//   const tokens = uuid4().split('-')
//   return tokens[2] + tokens[1] + tokens[0] + tokens[3] + tokens[4];
// }  

router.get("/sign_up", function(req, res, next) {
  res.render("user/signup");
});

function makeUuid4(){
  const tokens = uuid4().split('-')
  const uuid =  tokens[2] + tokens[1] + tokens[0] + tokens[3] + tokens[4];
  const uuidm = uuid.replace(/\-/g,'');
  return uuidm;
}

//회원가입
router.post("/sign_up", function(req, res, next) {
  let body = req.body;

  let inputPassword = body.pw;
  let salt = Math.round(new Date().valueOf() * Math.random()) + "";
  let hashPassword = crypto
    .createHash("sha512")
    .update(inputPassword + salt)
    .digest("hex");

  let token = Math.round(new Date().valueOf() * Math.random() * 12);
  
  const uuid = uuid4();
  const uuidinsert = uuid.replace(/\-/g,'');
  
  models.member
    .findOne({
      where: { mem_email: body.email }
    })
    .then(function(data) {
      //console.log(data);
      if ((data == null || data == undefined) == false) {
        // res.json({result:data});
        if (!data.mem_type) {
          models.member
            .update(
              {
                mem_pw: hashPassword,
                salt: salt,
                mem_token: token
              },
              {
                where: { mem_email: body.email }
              }
            )
            .then(result => {
              emailCheck(body.email, token);
              res.json({ result: true, message: "인증메일 재전송" });
            })
            .catch(err => {
              res.json({ result: false, message: err.message });
            });
        } else {
          res.json({ result: false, message: "회원가입 된 사용자" });
        }
      } else {
        models.member
          .create({
            mem_email: body.email,
            mem_pw: hashPassword,
            salt: salt,
            mem_token: token,
            mem_uuid :makeUuid4()
          })
          .then(result => {
            //res.redirect("/users/sign_up");
            emailCheck(body.email, token);
            //console.log(result11);
            res.json({ result: true, message: "success" });
            //res.redirect("/users/login")
          })
          .catch(err => {
            //console.log(err.errors);
            //res.send(err.message);
            res.json({ result: false, message: err.message });
          });
      }
    });
});

//email조회
router.get("/email/:email", function(req, res, next) {
  let email = req.params.email;
  models.member
    .findOne({
      where: { mem_email: email }
    })
    .then(function(data) {
      //console.log(data);
      if ((data == null || data == undefined) == false) {
        // res.json({result:data});
        if (!data.mem_type) {
          res.json({ result: true, message: "인증메일 재전송 가능" });
        } else {
          //res.send("인증됨");
          res.json({ result: false, message: "이미 사용중인 아이디 입니다." });
        }
      } else {
        res.json({ result: true, message: "사용 가능한 아이디 입니다." });
      }
    });
});

//nickname조회
router.get("/nick/:nick", function(req, res, next) {
  let nick = req.params.nick;
  models.member
    .findOne({
      where: { mem_nickname: nick }
    })
    .then(function(data) {
      console.log(data);
      if ((data == null || data == undefined) == false) {
        res.json({ result: false, message: "이미 사용중인 닉네임 입니다." });
      } else {
        res.json({ result: true, message: "사용 가능한 닉네임 입니다." });
      }
    });
});

//로그인 성공 화면 세션
router.get("/login", function(req, res, next) {
  let session = req.session;
  //console.log(session.email);

  if (session.email != null) {
    models.member
      .findOne({
        where: { mem_email: session.email }
      })
      .then(result => {
        //console.log(result)
        // console.log(result.mem_nickname)
        //session.info = result;
        //if(result.mem_nickname == null || result.mem_nickname == "" ){
        if (!result.mem_type) {
          res.json({ result: false, message: "이메일 인증이 필요합니다." });
        }

        if (
          !result.mem_nickname ||
          !result.mem_country ||
          !result.mem_state ||
          !result.mem_birth
        ) {
          //2차회원가입
          res.json({
            session: session,
            member: result
          });
        } else {
          //session.info = "y";
          //로그인 성공
          res.json({
            session: session,
            member: result
          });
        }
      })
      .catch(err => {
        console.log(err);
      });

    return;
  } else {
    res.render("user/login", {
      session: session
    });
    return;
  }
  if (session.email == null) {
    res.render("user/info", {
      session: session
    });
  }
});

//로그인 전송
router.post("/loginttt", function(req, res, next) {
  let body = req.body;

  console.log(body.email);

  models.member
    .findOne({
      where: { mem_email: body.email }
    })
    .then(result => {
      let dbPassword = result.dataValues.mem_pw;

      let inputPassword = body.pw;
      let salt = result.dataValues.salt;
      let hashPassword = crypto
        .createHash("sha512")
        .update(inputPassword + salt)
        .digest("hex");

      if (dbPassword === hashPassword) {
        console.log("비밀번호 일치");

        // 쿠키 설정
        // res.cookie("user", body.email , {
        //   expires: new Date(Date.now() + 900000),
        //   httpOnly: true
        // });

        // 세션 설정
        req.session.email = body.email;

        res.redirect("/users/login");
        //res.redirect("/users");
      } else {
        console.log("비밀번호 불일치");
        res.redirect("/users/login");
      }
    })
    .catch(err => {
      console.log(err);
    });
});

//로그아웃
router.get("/logout", function(req, res, next) {
  req.session.destroy();
  res.clearCookie("user");
  res.json({ resutl: true });
  //res.redirect("/users/login");
});

//추가정보 입력
router.post("/detail", function(req, res, next) {
  let body = req.body;
  models.member.findOne({
    where :{mem_email : body.email}
  })
  .then(function (data){
    if(data.mem_type){
      let re= updateInfo(body, res);
    }else{
      res.json({result:data.mem_type, message:"미승인 이메일 오류"});
    }
    
  })

});

router.get("/nodemailer", function(req, res, nest) {
  res.render("user/email");
});

function updateInfo(body, res){
  models.member
    .update(
      {
        mem_nickname: body.nickname,
        mem_country: body.country,
        mem_state: body.state,
        mem_birth: body.birth
      },
      {
        where: { mem_email: body.email }
      }
    )
    .then(result => {
      res.json({ result: true, message: result });
      
    })
    .catch(err => {
      res.json({ result: false, message: err });
    });
    
    
}

function emailCheck(email, token) {
  //let email = req.body.email;
  let result;
  let transporter = nodemailer.createTransport({
    service: "gmail",
    auth: {
      user: "leegm@gananetworks.com", // gmail 계정 아이디를 입력
      pass: "ganayo89" // gmail 계정의 비밀번호를 입력
    }
  });

  let mailOptions = {
    from: "youremail@gmail.com",
    to: email,
    subject: "안녕하세요, gana입니다. 이메일 인증을 해주세요.",

    html:
      "<p>아래의 링크를 클릭해주세요 !</p>" +
      "<a href='http://192.168.0.119:3000/users/auth/?email=" +
      email +
      "&token=" +
      token +
      "'>인증하기</a>"
  };

  transporter.sendMail(mailOptions, function(error, info) {
    if (error) {
      result = error;
      return result;
    } else {
      result = info.response;
      return result;
    }
  });
}

function emailCheckPw(email, token, res) {
  //let email = req.body.email;
  let result;
  let transporter = nodemailer.createTransport({
    service: "gmail",
    auth: {
      user: "leegm@gananetworks.com", // gmail 계정 아이디를 입력
      pass: "ganayo89" // gmail 계정의 비밀번호를 입력
    }
  });

  let mailOptions = {
    from: "youremail@gmail.com",
    to: email,
    subject: "안녕하세요, gana입니다.",

    html:
      "<p>임시 비밀번호가 발급되었습니다. 임시 비밀번호를 사용하시려면 아래의 링크를 클릭해주세요 !</p>" +
      "<a href='http://192.168.0.119:3000/users/chpw/?email=" +
      email +
      "&token=" +
      token +
      "'>인증하기</a>"
      + "<br>임시 비밀번호 : " +token
  };
  
  transporter.sendMail(mailOptions, function(error, info) {
    if (error) {
      res.json({result:false, message:"이메일 전송에러"+ errror});
    } else {
      chToken(email, token,res);
    }
  });
}

router.get("/auth", function(req, res, next) {
  let email = req.query.email;
  let token = req.query.token;
  models.member
    .findOne({
      where: { mem_email: email, mem_token: token }
    })
    .then(function(data) {
      if ((data == null || data == undefined) == false) {
        models.member
          .update(
            {
              mem_type: "1"
            },
            {
              where: {
                mem_email: email
              }
            }
          )
          .then(function(data) {
            res.json({ result: true, message: "인증성공" });
          });
      } else {
        res.json({ result: false, message: "인증실패" });
      }
    });
});


router.get("/chpw", function(req, res, next){
  let email = req.query.email;
  let token = req.query.token;
  models.member
    .findOne({
      where: { mem_email: email, mem_token: token }
    })
    .then(function(data) {
      if ((data == null || data == undefined) == false) {

        let inputPassword = token;
        let salt = Math.round(new Date().valueOf() * Math.random()) + "";
        let hashPassword = crypto
          .createHash("sha512")
          .update(inputPassword + salt)
          .digest("hex");

        models.member
          .update(
            {
              mem_pw: hashPassword,
              salt : salt
            },
            {
              where: {
                mem_email: email
              }
            }
          )
          .then(function(data) {
            res.json({ result: true, message: "pw변경" });
          });
      } else {
        res.json({ result: false, message: "인증실패" });
      }
    });
})


router.post("/updateinfo", function(req, res, next){
  let body = req.body;
  let inputPassword = body.pw;
  let salt = Math.round(new Date().valueOf() * Math.random()) + "";
  let hashPassword = crypto
    .createHash("sha512")
    .update(inputPassword + salt)
    .digest("hex");

  models.member
    .update(
      {
        mem_pw: hashPassword,
        salt : salt,
        mem_nickname: body.nickname,
        mem_country: body.country,
        mem_state: body.state,
        mem_birth: body.birth
      },
      {
        where: {
          mem_email: body.email
        }
      }
    )
    .then(function(data) {
      res.json({ result: true, message: "회원정보변경" });
    })
    .catch(function(err){
      res.json({ result: true, message:err});
    });
})

router.get("/test", function(req, res, next) {
  // default : HMAC SHA256
  let token = jwt.sign(
    {
      email: "foo@example.com" // 토큰의 내용(payload)
    },
    secretObj.secret, // 비밀 키
    {
      expiresIn: "5m" // 유효 시간은 5분
    }
  );
  res.send(token);
});

router.get("/tokencheck", function(req, res, next) {
  let token = req.cookies.user;
  console.log(token);
  if (token) {
    let decoded = jwt.verify(token, secretObj.secret);
    console.log(decoded);
    if (decoded) {
      res.json({ result: true, message: decoded });
    } else {
      res.json({ result: false, message: "token err" });
    }
  } else {
    res.json({ result: false, message: "login err" });
  }
});

//로그인 전송 jwt
router.post("/login", function(req, res, next) {
  let body = req.body;

  console.log(body.email);

  models.member
    .findOne({
      where: { mem_email: body.email }
    })
    .then(result => {
      console.log(result);
      let dbPassword = result.dataValues.mem_pw;
      let inputPassword = body.pw;
      let salt = result.dataValues.salt;
      let hashPassword = crypto
        .createHash("sha512")
        .update(inputPassword + salt)
        .digest("hex");

      if (dbPassword === hashPassword) {
        console.log("비밀번호 일치");
        let token = jwt.sign(
          {
            email: body.email // 토큰의 내용(payload)
          },
          secretObj.secret, // 비밀 키
          {
            expiresIn: "60d" // 유효 시간은 60일
          }
        );

        //TODO: 요구
        res.cookie("user", token);

        req.session.ganatoken = token;

        res.json({ result: true, message: token });
      } else {
        res.json({ result: false, message: "login err" });
      }
    })
    .catch(err => {
      res.json({ result: false, message: "email 확인"+ err });
    });
});



router.post("/newpw", function(req, res, next){
  let body = req.body;
  models.member.findOne({
    where :{mem_email : body.email}
  })
  .then(function(data){
    let token = Math.round(new Date().valueOf() * Math.random() * 12);
    //chToken(body.email, token, res);    
    emailCheckPw(body.email, token, res);
    
    //console.log("1212");
    //res.send(re + "?///");
  })
  .catch(err => {
    res.json({ result: false, message: err });
  });
})

function chToken(email, token, res){
  models.member.update({
    mem_token : token
  },
  {
  where:{
    mem_email:email
  }
})
.then(result =>{
  res.json({result:true,message:"이메일 전송 및 토큰 정보 수정"})
}).catch(err =>{
  res.json({result:false, message:"토큰 변경 에러"+ err})
})

}



router.get("/chpwTest", function(req, res, next){
  let email = req.query.email;
  let pw = req.query.pw;
  let inputPassword = pw;
  let salt = Math.round(new Date().valueOf() * Math.random()) + "";
  let hashPassword = crypto
    .createHash("sha512")
    .update(inputPassword + salt)
    .digest("hex");

  models.member
    .update(
      {
        mem_pw: hashPassword,
        salt : salt
      },
      {
        where: {
          mem_email: email
        }
      }
    )
    .then(function(data) {
      res.json({ result: true, message: "pw변경" });
    })
    .catch(function(err){
      res.json({ result: true, message:err});
    });


})



router.get("/test3", (req, res, next) => {
    
  // var vvvv = findOneEmail()
      
  //     res.send("+++"  + vvvv.email);
  //     console.log("55555555555555")
  //  res.send("+++" );
  console.log("1");
  
  /*findOneEmail().then(result=>{
      console.log("2");
      console.log(result + "////")
  });*/
  findOneEmail().then(result => {
      // console.log(res)
      res.send("+++"+result.mem_email  );
      console.log("55555555555555")
      //res.send("+++" );
  })
  .catch(console.log);
  console.log("3");
  // res.send("+++");
  
})

const findOneEmail = /*async*/ () =>{

  return models.member.findOne({
      where: { mem_email: "blog_halee@naver.com" }
    });
  //   .then( result => {
  //     console.log("5");
  //       //console.log(result.mem_email);
  //       //return result.mem_email + "m,.m";
  //       a = result.mem_email
  //     //   console.log(a + "333");
        
  //       return new Promise((resolve, reject) => {
  //         resolve(result);
  //         // console.log("444" + result.mem_email)
  //         console.log("6");
  //     });
  //   }).catch(err =>{
  //     //return err;
  //   })
  //   console.log("7");
  //   console.log("2222222222")
}




router.get("/test3", (req, res, next) => {
  console.log("1");

//   util
//     .findOneEmail("123")
findOneEmail1("123")
    .then(result => {
      // console.log(res)
      if (!result) {
        res.send("zzz");
      } else {
        res.send("+++" + result + "////");
      }
    })
    .catch(err => {
      res.send(err + "");
    });
});

  function findOneEmail1(email){
    return models.member.findOne({
        where: { mem_email: email}
      });
  }


router.get("/test4", (req, res, next) => {
    findOneEmail2().then(result=>{
        if(result){
            res.send(result);
        }else{
            res.send("////");
        }
    });
  });

function findOneEmail2() {
    let result =  new Promise((resolve, reject)=> {
        resolve(models.member
            .findOne({ where: { mem_email: "123"} }))
    })

    return result;
}


//회원가입
router.post("/sign_up", function(req, res, next) {
  let body = req.body;

  let inputPassword = body.pw;

  let salt = Math.round(new Date().valueOf() * Math.random()) + "";

  let hashPassword = crypto
    .createHash("sha512")
    .update(inputPassword + salt)
    .digest("hex");

  let token = Math.round(new Date().valueOf() * Math.random() * 12);

  models.member
    .findOne({
      where: { mem_email: body.email }
    })
    .then(function(data) {
      //console.log(data);
      if ((data == null || data == undefined) == false) {
        // res.json({result:data});
        if (!data.mem_type) {
          models.member
            .update(
              {
                mem_pw: hashPassword,
                salt: salt,
                mem_token: token
              },
              {
                where: { mem_email: body.email }
              }
            )
            .then(result => {
              util.emailCheck(body.email, token);
              res.json({ result: true, message: "인증메일 재전송" });
            })
            .catch(err => {
              res.json({ result: false, message: err.message });
            });
        } else {
          res.json({ result: false, message: "회원가입 된 사용자" });
        }
      } else {
        models.member
          .create({
            mem_email: body.email,
            mem_pw: hashPassword,
            salt: salt,
            mem_token: token,
            mem_uuid: util.makeUuid4(),
            sns_type: 0
          })
          .then(result => {
            util.emailCheck(body.email, token);
            res.json({ result: true, message: "success" });
          })
          .catch(err => {
            res.json({ result: false, message: err.message });
          });
      }
    });
});

module.exports = router;
