require('dotenv').config();
let models = require("./models/index.js");

models.sequelize.sync().then( () => {
  console.log(" DB 연결 성공")
}).catch(err => {
  console.log("연결 실패")
  console.log(err)
})
const cors = require('cors');
var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var debug = require('debug')('restapi:server');
var session = require('express-session');


var app = express();
var indexRouter = require('./routes')(app);



//보안
var helmet = require('helmet');
app.use(helmet());
app.use(helmet.xssFilter());
app.use(helmet.noSniff());
app.use(helmet.frameguard("deny"));
app.disable('x-powered-by');

//스케쥴러 탈퇴 미인증회원 정리
const cron = require('node-cron');
const scheduleDrop = require('./routes/v1/schecule');

cron.schedule('0 0 * * * *', () =>{
  console.log('schedule')
  scheduleDrop.findDropMember();
  scheduleDrop.dropType0Member();
})


app.use(cors());
//app.glob();
app.use(session({
  key: 'sid', 
  secret: 'secret',
  resave: false,
  saveUninitialized: true,
  cookie: {
    maxAge: 24000 * 60 * 60 // 쿠키 유효기간 24시간
  }
}))

// ,
//   store : new FileStore()


// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

//app.use(logger('dev'));
if(process.env.NODE_ENV !== 'test'){
  app.use(logger('dev'));
}

app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use('/', indexRouter);



//파일읽기
app.use('/profile' , express.static('profile'))
app.use('/certification' , express.static('certification'))
app.use('/postImg' , express.static('postImg'))

 

//로그인 보안 - session 뒤에
const passport = require('passport')
  , LocalStrategy = require('passport-local').Strategy
  , FacebookStrategy = require('passport-facebook').Strategy;

// const flash = require('connect-flash');
// const async = require('async');
// app.use(flash());
 app.use(passport.initialize());
 app.use(passport.session());
 const passportConfig = require('./routes/v1/passport');
 passportConfig(passport);

 

//버전1 정리
var v1Router = require('./routes/v1')(app);
app.use('/users' , v1Router);

// app.get('/users/facebook', passport.authenticate('facebook', {
//   scope:'email'
// }));
// app.get('/users/facebook/callback', passport.authenticate('facebook', { successRedirect : '/',
//                                                                          failureRedirect:'/users/login'}));

//파일읽기
app.use('/profile' , express.static('profile'));
app.use('/certification' , express.static('certification'));



// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
