'use strict';
module.exports = (sequelize, DataTypes) => {
  const support = sequelize.define('support', {
    sUuid:{
      allowNull :false,
      type: DataTypes.UUID,
      defaultValue:DataTypes.UUIDV4
    },
    title:{
      type: DataTypes.STRING
    },
    email: {
      type: DataTypes.STRING
    },
    name: {
      type: DataTypes.STRING
    },
    description:{
      type: DataTypes.STRING(3000)
    }
  }, {});
  support.associate = function(models) {
    // associations can be defined here
  };
  return support;
};