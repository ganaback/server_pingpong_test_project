'use strict';
module.exports = (sequelize, DataTypes) => {
  const vote = sequelize.define('vote', {
    
    voteUuid: {
      allowNull :false,
      // unique : true,
      type: DataTypes.UUID,
      defaultValue:DataTypes.UUIDV4
    },
    voteType: {
      type: DataTypes.INTEGER
    },
    postUuid: {
      type: DataTypes.STRING
    },
    userUuid:{
      type: DataTypes.STRING
    }
  }, {});
  vote.associate = function(models) {
    // associations can be defined here
  };
  return vote;
};