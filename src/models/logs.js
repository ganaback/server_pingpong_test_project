'use strict';
module.exports = (sequelize, DataTypes) => {
  const log = sequelize.define('log', {
    
    logUuid: {
      allowNull :false,
      type: DataTypes.UUID,
      defaultValue:DataTypes.UUIDV4
    },
    logType: {
      type: DataTypes.INTEGER(3)
    },
    userUuid:{
      type: DataTypes.STRING
    },
    tableKey: {
        type: DataTypes.STRING
    },
    contents:{
        type: DataTypes.STRING(3000)
    },
    userIp:{
        type: DataTypes.STRING(128)
    },
    category:{
        type: DataTypes.STRING(512)
    },
    receiveId:{
      type: DataTypes.STRING(255)
    },
    read_at:{
        type: DataTypes.DATE

    },
    linkId:{
      type: DataTypes.STRING(36)
    }
  }, {});
  log.associate = function(models) {
    // associations can be defined here
  };
  return log;
};