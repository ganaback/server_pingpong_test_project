'use strict';
module.exports = (sequelize, DataTypes) => {
  const file = sequelize.define('file', {
    mem_uuid: DataTypes.STRING,
    postUuid: DataTypes.STRING,
    fiUuid: {
      allowNull :false,
      type: DataTypes.UUID,
      defaultValue:DataTypes.UUIDV4
    },
    fiOriginname: DataTypes.STRING,
    fiFilename :  DataTypes.STRING,
    fiSize: DataTypes.INTEGER,
    fiType: DataTypes.STRING,
    fiDestination: DataTypes.STRING,
    fiUsedType: DataTypes.INTEGER,
    supportId:DataTypes.STRING(36)


    //fiIsImage: DataTypes.STRING,

  }, {});
  file.associate = function(models) {
    // associations can be defined here
    // file.belongsTo(models.member,
    //   {foreignKey: 'memUuid', targetKey:"mem_uuid"}
    // )

    // file.belongsTo(models.member,
    //   {foreignKey: 'fiUuid', targetKey: 'mem_profileBg'}
    // )
  };
  return file;
};