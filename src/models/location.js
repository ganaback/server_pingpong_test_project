'use strict';
module.exports = (sequelize, DataTypes) => {
  const location = sequelize.define('location', {
    mem_uuid: {
      type: DataTypes.STRING
    },
    locaUuid: {
      allowNull :false,
      type: DataTypes.UUID,
      defaultValue:DataTypes.UUIDV4
    },   
    locationName: {
      type: DataTypes.STRING
    },
    startYear: {
      type: DataTypes.DATE
    },
    endYear: {
      type: DataTypes.DATE
    },
    isLive: {
      type: DataTypes.BOOLEAN
    }
   
  }, {});
  location.associate = function(models) {
    // associations can be defined here
  };
  return location;
};