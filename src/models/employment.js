'use strict';
module.exports = (sequelize, DataTypes) => {
  const employment = sequelize.define('employment', {
    mem_uuid: {
      type: DataTypes.STRING
    },
    empUuid: {
      allowNull :false,
      type: DataTypes.UUID,
      defaultValue:DataTypes.UUIDV4
    },
   
    position: {
      type: DataTypes.STRING
    },
    company: {
      type: DataTypes.STRING
    },
    startYear: {
      type: DataTypes.DATE
    },
    endYear: {
      type: DataTypes.DATE
    },
    isWork: {
      type: DataTypes.BOOLEAN
    }
   
  }, {});
  employment.associate = function(models) {
    // associations can be defined here
  };
  return employment;
};