'use strict';
module.exports = (sequelize, DataTypes) => {
  const article = sequelize.define('article', {
    
    articleUuid: {
      allowNull :false,
      type: DataTypes.UUID,
      defaultValue:DataTypes.UUIDV4
    },
   
    body: {
      type: DataTypes.STRING(3000)
    },
    title: {
      type: DataTypes.STRING
    },
    mem_uuid: {
      type: DataTypes.STRING
    }
  }, {});
  article.associate = function(models) {
    // associations can be defined here
  };
  return article;
};