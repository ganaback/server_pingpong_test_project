'use strict';
module.exports = (sequelize, DataTypes) => {
  const category = sequelize.define('category', {
    categoryName: {
      unique : true,
      type: DataTypes.STRING
    }
    // categoryLink: DataTypes.STRING,
    // categoryParentId: DataTypes.STRING,
    // sortOrder: DataTypes.STRING
  }, {});
  category.associate = function(models) {
    // associations can be defined here
  };
  return category;
};