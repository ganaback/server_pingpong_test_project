'use strict';
module.exports = (sequelize, DataTypes) => {
  const member = sequelize.define('member', {
    
    mem_email: {
      allowNull :false,
      unique : true,
      type: DataTypes.STRING,
      validate :{
        isEmail:true,
        notEmpty: true
      }
    },
    mem_pw: {
      allowNull :false,
      type: DataTypes.STRING,
      // validate :{        
      //   notEmpty: true
      // }
    },
    salt: {
      type: DataTypes.STRING,
      comment : "pw"
    },
    mem_nickname: {
      unique : true,
      type: DataTypes.STRING
    },
    mem_country: {
      type: DataTypes.STRING
    },
    mem_state: {
      type: DataTypes.STRING
    },
    mem_birth: {
      type: DataTypes.DATE
    },
    mem_type: {
      type: DataTypes.STRING
    },
    mem_token: {
      type: DataTypes.STRING
    },
    mem_uuid :{
      type : DataTypes.STRING
    },
    mem_profile :{
      type : DataTypes.STRING
    },
    mem_profileBg :{
      type : DataTypes.STRING
    },
    sns_type:{
      type : DataTypes.INTEGER
    },
    budtender:{
      type : DataTypes.INTEGER
    },
    mem_droptime:{
      type : DataTypes.DATE
    },
    mem_info :{
      type : DataTypes.STRING
    },
    mem_interested :{
      type : DataTypes.STRING
    },
    mem_links :{
      type : DataTypes.STRING
    },
    mem_sub_email :{
      type : DataTypes.STRING
    },
    mem_notifications :{
      type : DataTypes.STRING
    },
    mem_sns_open:{
      type : DataTypes.INTEGER
    },
    mem_search_agree:{
      type : DataTypes.INTEGER
    },
    mem_use_data_agree:{
      type : DataTypes.INTEGER
    }
  }, {});
  member.associate = function(models) {
    // associations can be defined here
    // member.belongsTo(models.file,
    //   {foreignKey: 'mem_profile', targetKey: 'fiUuid'});
    // };
    // member.belongsTo(models.file,
    //   {foreignKey: 'mem_profileBg', targetKey: 'fiUuid'});
    // };
    
  }
  return member;
};