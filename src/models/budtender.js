'use strict';
const models = require("./index");

module.exports = (sequelize, DataTypes) => {
  

  const budtender = sequelize.define('budtender', {
    mem_uuid: {
     // unique : true,
      type: DataTypes.STRING
      // ,
      ,allowNull: false
     
    },
    articleUuid: DataTypes.STRING,
    budMessage: DataTypes.STRING,
    budCode: DataTypes.STRING,
    budState: DataTypes.INTEGER,
    fiUuid: DataTypes.STRING,
  }, {});
  budtender.associate = function(models) {
    // associations can be defined here
   
  };
  return budtender;
};