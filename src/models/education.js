'use strict';
module.exports = (sequelize, DataTypes) => {
  const education = sequelize.define('education', {
    mem_uuid: {
      type: DataTypes.STRING
    },
    eduUuid: {
      allowNull :false,
      type: DataTypes.UUID,
      defaultValue:DataTypes.UUIDV4
    },
    school: {
      type: DataTypes.STRING
    },
    major: {
      type: DataTypes.STRING
    },
    degree: {
      type: DataTypes.STRING
    },
    graduationYear: {
      type: DataTypes.DATE
    }
  }, {});
  education.associate = function(models) {
    // associations can be defined here
  };
  return education;
};