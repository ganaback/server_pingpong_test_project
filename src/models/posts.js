'use strict';
module.exports = (sequelize, DataTypes) => {
  const post = sequelize.define('post', {
    
    postUuid: {
      allowNull :false,
      // unique : true,
      type: DataTypes.STRING,
      validate :{
        notEmpty: true
      }
    },
    acceptedAnswerId: {
      type: DataTypes.STRING,
    },
    parentId: {
      type: DataTypes.STRING,
    },
    voteCount: {
      type: DataTypes.INTEGER,
      defaultValue: 0
    },
    viewCount: {
      type: DataTypes.INTEGER,
      defaultValue: 0
    },
    body: {
      type: DataTypes.STRING(3000)
    },
    title: {
      type: DataTypes.STRING
    },
    ownerUserId: {
      type: DataTypes.STRING
    },
    ownerUserName: {
      type: DataTypes.STRING
    },
    answerCount:{
      type : DataTypes.INTEGER,
      defaultValue: 0
    },
    closedDate:{
        type:DataTypes.DATE
    },
    category:{
      type : DataTypes.STRING(512)
    }
  }, {});
  post.associate = function(models) {
    // associations can be defined here
  };
  return post;
};