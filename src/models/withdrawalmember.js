'use strict';
module.exports = (sequelize, DataTypes) => {
  const withdrawalmember = sequelize.define('withdrawalmember', {
    
    mem_email: {
      
      type: DataTypes.STRING
      
    },
    mem_pw: {
    
      type: DataTypes.STRING
    
    },
    salt: {
      type: DataTypes.STRING,
      comment : "pw"
    },
    mem_nickname: {
     // unique : true,
      type: DataTypes.STRING
    },
    mem_country: {
      type: DataTypes.STRING
    },
    mem_state: {
      type: DataTypes.STRING
    },
    mem_birth: {
      type: DataTypes.DATE
    },
    mem_type: {
      type: DataTypes.STRING
    },
    mem_token: {
      type: DataTypes.STRING
    },
    mem_uuid :{
      type : DataTypes.STRING
    },
    sns_type:{
      type : DataTypes.INTEGER
    },
    answer:{
      type : DataTypes.INTEGER
    },
    mem_droptime:{
      type : DataTypes.DATE
    },
    mem_jointime :{
      type : DataTypes.DATE
    },
    mem_profile :{
      type : DataTypes.STRING
    },
    mem_profileBg :{
      type : DataTypes.STRING
    },
    mem_info :{
      type : DataTypes.STRING
    },
    mem_interested :{
      type : DataTypes.STRING
    },
    mem_links :{
      type : DataTypes.STRING
    },
    mem_sub_email :{
      type : DataTypes.STRING
    },
    mem_last_updatedAt :{
      type : DataTypes.DATE
    },
    mem_notifications :{
      type : DataTypes.STRING
    },
    mem_sns_open:{
      type : DataTypes.INTEGER
    },
    mem_search_agree:{
      type : DataTypes.INTEGER
    },
    mem_use_data_agree:{
      type : DataTypes.INTEGER
    }
  }, {});
  withdrawalmember.associate = function(models) {
    // associations can be defined here
  };
  return withdrawalmember;
};