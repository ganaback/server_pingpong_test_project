import uuid4 from 'uuid4'

const uuid = () => {
    const orderedUuid = uuid()

    return expect(orderedUuid).toMatch(/\b4[0-9A-Fa-f]{31}\b/g)
    
}

export {
  uuid
}
