import jwt from 'jsonwebtoken'

module.exports.verify  =(token)=>{
    const payload = jwt.verify(token, process.env.JWT_SECRET);
    return payload;
}

