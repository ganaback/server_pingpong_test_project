const multer = require("multer");
const path = require("path");
const thumb = require('node-thumbnail').thumb;
const fs = require("fs");
const filesql = require("../routes/v1/sql/file_sql")
//저장소 = 인증서
let certification = multer.diskStorage({
    destination: function(req, file, callback) {
      callback(null, "certification/");
    },
    filename: function(req, file, callback) {
      let extension = path.extname(file.originalname);
      let basename = path.basename(file.originalname, extension);
      callback(null, basename + "-" + Date.now() + extension);
    }
  });

  //저장소 = 프로필
let profile = multer.diskStorage({
    destination: function(req, file, callback) {
      callback(null, "profile/");
    },
    filename: function(req, file, callback) {
      let extension = path.extname(file.originalname);
      let basename = path.basename(file.originalname, extension);
      callback(null, basename + "-" + Date.now() + extension);
    }
  });



let postStorage = multer.diskStorage({
    destination: function(req, file, callback) {
      callback(null, "postImg/");
    },
    filename: function(req, file, callback) {
      let extension = path.extname(file.originalname);
      let basename = path.basename(file.originalname, extension);
      callback(null, basename + "-" + Date.now( ) + extension);
    }
});



//이미지만
const fileFilterImg = function(req, file, cb){
    let typeArray = file.mimetype.split('/');
    let fileType = typeArray[1];
    if(fileType =='jpg' ||
    fileType =='png' ||
    fileType =='jpeg' ||
    fileType =='gif' ){
      cb(null, true)
    }else{
      cb(null, false)
      req.fileValidationError = "jpg,png,jpeg,gif 파일만 업로드 가능";
    }
  }

  
  





  //이미지 + pdf
  const fileFilterCer = function(req, file, cb){
    let typeArray = file.mimetype.split('/');
    let fileType = typeArray[1];
    // console.log(req.files)
    // console.log(req.files.length)
    // console.log("====================")
    if(fileType =='jpg' ||
    fileType =='png' ||
    fileType =='jpeg' ||
    fileType =='gif' ||
    fileType =='pdf'){
      cb(null, true)
    }else{
      cb(null, false)
      req.fileValidationError = "jpg,png,jpeg,gif,pdf 파일만 업로드 가능";
    }
    
  }


  let img = multer({
    storage: profile,
    fileFilter : fileFilterImg
  });
  

let imgpdf = multer({
    storage: certification,
    fileFilter : fileFilterCer,
    limits : {files :3}
  });

let postImg = multer({
    storage:postStorage,
    fileFilter : fileFilterImg
});

 

  //img 한장만
module.exports.profileupload  =()=>{
    return img;
}

//증명서 3장까지, img+pdf
module.exports.certification  =()=>{
    return imgpdf;
}

module.exports.postImg =()=>postImg;

module.exports.thumb=(file)=>{
  return thumb({
    suffix:'_thumb',
    source: file.path,
    destination: file.destination,
    width: 80
  })
}

  module.exports.deletefile = (file)=>{
    //원본삭제
     fs.unlink(file.fiDestination + file.fiFilename, err => {
       if(err){
        console.log(err);
       }else{
        console.log("삭제완료");
       }
      
    })
  }

  module.exports.deletefileOnfile = (file)=>{
    //원본삭제
    //console.log(file);
     fs.unlink(file, err => {
       if(err){
        console.log(err);
       }else{
        console.log("삭제완료");
       }
      
    })
  }

  module.exports.deletefilethumb = (file)=>{
    let filename=file.fiFilename;
    const idx =filename.lastIndexOf(".");
    filename = filename.substring(0, idx) + "_thumb" + filename.substring(idx , filename.length);
   //썸네일삭제
   fs.unlink(file.fiDestination + filename , err => {
    if(err){
      console.log(err);
     }else{
      console.log("썸네일삭제완료");
     }
   });
 }
  

//  module.exports.deletefileuuid = (fileuuid)=>{
//   //원본삭제

//     filesql.findOneFiUuid(fileuuid)
//     .then(file=>{
//       fs.unlink(file.fiDestination + file.fiFilename, err => {
//         if(err){
//          console.log(err);
//         }else{
//          console.log("삭제완료");
//         }
//      })
//     })
//     .catch(err=>{
//       console.log(err)
//     })

  
// }