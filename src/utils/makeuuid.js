import uuid4 from 'uuid4'
module.exports.makeuuid  =()=>{
    const tokens = uuid4().split('-')
    const uuid =  tokens[2] + tokens[1] + tokens[0] + tokens[3] + tokens[4];
    const uuidm = uuid.replace(/\-/g,'');
    return uuidm;
  }

