
/**
 * Created by wedul on 2018. 8. 30.
 */
'use strict';
 
const winston = require('winston');
require('date-utils');
const fs = require('fs');
const logDir = 'logFile';
const tsFormat = () => (new Date()).toLocaleTimeString();

class Logger {
 
  create() {
    if (!fs.existsSync(logDir)) {
        fs.mkdirSync(logDir);
      }
   
    const logger = winston.createLogger({
  
      transports: [
        new (winston.transports.Console)({
        /*  timestamp: tsFormat, */
         colorize: true,
         level: 'info'
       }),
        new (require('winston-daily-rotate-file'))({
          level: 'info',
          filename: `${logDir}/%DATE%-logs.log`,

          datePattern: 'YYYY-MM-DD',
          prepend: true,
        })
      ]
    });
 
    // 운영중이지 않을 경우 콘솔에 출력 추가
    if (process.env.NODE_ENV !== 'production') {
      logger.add(new winston.transports.Console({
        format: winston.format.simple()
      }));
    }
 
    return logger;
  }
};

Logger.streamError = {
    write (message) {
        Logger.info(`Express Request Error: ${message}`)
    }
  }
  
 
module.exports = new Logger();

